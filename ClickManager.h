#ifndef CLICKMANAGER_H
#define CLICKMANAGER_H

#include <SFML/Graphics.hpp>
#include "Entity.h"

namespace cotw {

class ClickManager
{
	public:
		ClickManager();
		~ClickManager();

		std::vector<Entity*> clickables;
		void handleClick(sf::Vector2i);
};

}

#endif
