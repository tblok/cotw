#ifndef TILESET_H
#define TILESET_H

#include "Tile.h"
#include "Pos.h"
#include "TextureManager.h"

namespace cotw {

class Tileset
{
	public:
		Tileset();
		~Tileset();
		std::vector<std::vector<cotw::Tile*>> tiles;

	protected:
		TextureManager textureManager;
		sf::IntRect *boundingBox;
};

}

#endif
