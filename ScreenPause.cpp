#include <iostream>
#include "ScreenPause.h"

using namespace std;

namespace cotw {

ScreenPause::ScreenPause(sf::Texture& _texture, std::string _text, sf::Vector2f coords): visible(false)
{
	texture = _texture;
	//font.loadFromFile("DejaVuSans.ttf");
	sf::Vector2f text_coords = coords;
	text_coords.y += 8;
	text_coords.x += 10;
	name = "ScreenPause";

	sprite.setTexture(texture);
	sprite.setPosition(coords); 
}

ScreenPause::~ScreenPause(){}

void ScreenPause::update() 
{

}

void ScreenPause::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{
	if (visible)
		render_target.draw(sprite, render_states);
}

void ScreenPause::onClick(cotw::GameState* current_game_state)
{
	current_game_state->changeState(current_game_state->gameStateManager, cotw::State::MAINMENU);
}

}
