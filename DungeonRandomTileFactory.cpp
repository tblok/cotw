#include <iostream>
#include "DungeonRandomTileFactory.h"

using namespace std;

namespace cotw {

DungeonRandomTileFactory::DungeonRandomTileFactory() 
{

}

DungeonRandomTileFactory::~DungeonRandomTileFactory()
{

}

std::vector<std::vector<cotw::Tile*>> DungeonRandomTileFactory::createTiles(const int row, const int col)
{
	const std::string random_texture_name = getRandomTileType();
	std::string base_texture = "Grass1";
	sf::Texture base_texture_grass = textureManager.getTexture(base_texture, cotw::Atlas::game);
	bool blocking;
	bool retryBecauseTilesetCollides;
	std::vector<std::vector<cotw::Tile*>> tileset;

	do
	{
		retryBecauseTilesetCollides = false;

		if (random_texture_name == "somethingInDungeon")
			blocking = false;
		else 
		{
			sf::Texture texture;

			if (random_texture_name == "Grass1")
			{
				blocking = false;
				texture = textureManager.getTexture(base_texture, cotw::Atlas::game);
			}
			else
			{
				texture = textureManager.getMergedTexture(base_texture, random_texture_name);
			}

			// Wrap the singular texture in a vector to comply with method signature...
			tileset =
			{ 
				{
					new cotw::Tile(random_texture_name, texture, row * 32, col * 32, blocking)
				}
			};
		}
	}
	while (retryBecauseTilesetCollides);

	return tileset;
}

}
