#ifndef MAPP_H
#define MAPP_H

#include <algorithm> 
#include <vector>
#include <SFML/Graphics.hpp>
#include "TextureManager.h"
#include "Tile.h"
#include "Tileset.h"
#include "Log.h"
#include "Item.h"
#include "JsonReader.h"
#include "TileFactory.h"
#include "OutsideRandomTileFactory.h"
#include "DungeonRandomTileFactory.h"

namespace cotw {

enum MapType { OUTSIDE, LV1DUNG, LV2DUNG, LV3DUNG };

class Mapp
{
	public:
		Mapp(unsigned int);
		~Mapp();
		
		// Access with [row][column]
		std::array<std::array<cotw::Drawable*, 31>, 31> tiles;

		void create();
	private:
		int level;
		cotw::TileFactory* tileFactory;
		std::array<sf::IntRect, 10> rooms;
		std::map<std::string, float> tilesetSpawnrates;
		cotw::TextureManager textureManager;

		void createOutside();
		void createDungeon();
		void placeTiles(const std::vector<std::vector<cotw::Tile*>>&, const unsigned int, const unsigned int);

		//int getRandomIntegerBetween(int, int);
		void initializeSpawnRateMap();
		//void createDungeon(cotw::MapType);

		void createRooms();
		void createRoom(sf::IntRect, int);
		void overlaySpecialTiles();

		void digTunnels();
		void createTunnel(sf::IntRect);
		void createHTunnel(int, int, int);
		void createVTunnel(int, int, int);

		void createUniqueTile(std::string, std::string);
		const std::vector<sf::IntRect> getUnconnectedRooms();
		const std::vector<std::vector<cotw::Tile*>> factory(const std::string, int, int);
};

}

#endif
