#ifndef MAPTESTS_H
#define MAPTESTS_H

#include <cppunit/extensions/HelperMacros.h>
#include "Map.h"

class MapTests: public CPPUNIT_NS::TestFixture 
{
	CPPUNIT_TEST_SUITE( MapTests ); // Note 3 

	CPPUNIT_TEST( collision );

	CPPUNIT_TEST_SUITE_END();

	public:
		void setUp();
		void tearDown();

	protected:

	private:
		cotw::Map map;
		void collision();
		// I want to test if all tile_spawnrates items have a match with tiles in the json file.
};

#endif
