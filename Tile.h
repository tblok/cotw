#ifndef TILE_H
#define TILE_H

#include <iostream>
#include "Drawable.h"
#include "Item.h"

namespace cotw {

class Tile: public cotw::Drawable 
{
	public:
		//Tile(sf::Image&, int, int, bool);
		Tile();
		Tile(std::string, sf::Texture&, int, int, bool);
		~Tile();
		sf::Texture originalTexture;
		//sf::Texture fogTexture;
		//sf::Image fogImg;

		bool blocking = false;
		std::string name;
		std::vector<cotw::Item*> inventory;

		void update();
		void overlayTexture(const sf::Image&);
		void draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const;
		void addToInventory(cotw::Item*);
		void removeFromInventory();
		void interact();
		void conceal();
		void reveal();
		//void move(sf::Vector2i);

};

}

#endif
