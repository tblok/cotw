#include "ExperimentalButton.h"

namespace cotw {

ExperimentalButton::ExperimentalButton() 
{
}

ExperimentalButton::ExperimentalButton(sf::Texture& _texture, std::string _text, sf::Vector2f position, sf::Vector2u texSize) 
{
	texture = _texture;

	font.loadFromFile("res/8bit.ttf");
	text.setFont(font);
	text.setCharacterSize(30);
	text.setStyle(sf::Text::Bold);
	text.setFillColor(sf::Color::White);
	text.setString(_text);

	sprite.setTexture(texture);
	sprite.setPosition(position); 
}

ExperimentalButton::~ExperimentalButton() {}

void ExperimentalButton::onClick(cotw::GameState* current_game_state)
{
	cotw::InGameState *current_ingame_state;
	current_ingame_state = static_cast<cotw::InGameState*>(current_game_state);

	if (current_ingame_state->screenInventory->visible)
		current_ingame_state->screenInventory->visible = false;
	else
		current_ingame_state->screenInventory->visible = true;
}

void ExperimentalButton::update()
{

}

}
