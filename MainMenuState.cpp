#include <iostream>
#include "MainMenuState.h"
#include "GameStateManager.h"

using namespace std;

namespace cotw {

extern int windowSizeH;
extern int windowSizeV;

MainMenuState::MainMenuState(cotw::GameStateManager* _gameStateManager, sf::RenderWindow* _window) 
{
	gameStateManager = _gameStateManager;
	window = _window;

	// ######################## experimental code
	sf::Texture tex_button = textureManager.getTexture("gui/button3.png", cotw::Atlas::ui);

	float smx = windowSizeH / 2;
	float smy = windowSizeH / 2;

	cotw::Entity *bStart = new cotw::ExperimentalButton
	(
		tex_button,
		"Start",
		sf::Vector2f((smx - tex_button.getSize().x / 2), ((smy - tex_button.getSize().y / 2) - tex_button.getSize().y) - 5),
		tex_button.getSize()
	);

	drawingManager.drawables.push_back(bStart);
	clickManager.clickables.push_back(bStart);
	// ######################## END experimental code
	
	// After creation of the drawables, we also need to remember all UI elements as Clickables in the ClickableManager.
	
	//void (*foobar)();
	//foobar = &cotw::MainMenuState::callback;
	
	// works but...
	//void (cotw::MainMenuState::*myCallback)() = &cotw::MainMenuState::callback;
	//auto myCallback = std::bind(&cotw::MainMenuState::callback, NULL);
	//myCallback();

	setupUi();
}

MainMenuState::~MainMenuState() {}

void MainMenuState::callback()
{
	std::cout << "Callback works!" << std::endl;
}

void MainMenuState::setupUi()
{
	float smx = windowSizeH / 2;
	float smy = windowSizeH / 2;

	sf::Texture *bg_texture = new sf::Texture(textureManager.getTexture("gui/bg.png", cotw::Atlas::ui));
	background.setTexture(*bg_texture);
	background.setPosition(sf::Vector2f(0, 0)); 

	sf::Texture tex_button = textureManager.getTexture("gui/button3.png", cotw::Atlas::ui);

	//cotw::Button *button_start = new cotw::ButtonStart(tex_button, "Start", sf::Vector2f((smx - tex_button.getSize().x / 2), ((smy - tex_button.getSize().y / 2) - tex_button.getSize().y) - 5), tex_button.getSize().x, tex_button.getSize().y);

	cotw::Button *button_settings = new cotw::ButtonSettings(tex_button, "Settings", sf::Vector2f((smx - tex_button.getSize().x / 2), (smy - tex_button.getSize().y / 2)), tex_button.getSize().x, tex_button.getSize().y);

	cotw::Button *button_quit = new cotw::ButtonQuit(tex_button, "Quit", sf::Vector2f((smx - tex_button.getSize().x / 2), ((smy - tex_button.getSize().y / 2) + tex_button.getSize().y) + 5), tex_button.getSize().x, tex_button.getSize().y);

	//uiElements[0] = button_start;
	uiElements[0] = button_settings;
	uiElements[1] = button_quit;
}

void MainMenuState::changeState(cotw::GameStateManager* gameStateManager, cotw::State state)
{
	gameStateManager->setState(state);
}


void MainMenuState::update() 
{
	sf::Event event;

	while (window->pollEvent(event))
		handleKey(window, event);
}

void MainMenuState::handleKey(sf::RenderWindow* window, sf::Event event)
{
	sf::Vector2f new_move;

	switch (event.type) 
	{
		case sf::Event::KeyPressed:
		{
			switch (event.key.code) 
			{
				case sf::Keyboard::Return:
				{
					//std::cout << "Bye." << std::endl;
					//cotw::InGameState *foo = new InGameState(gameStateManager);
					cout << "MainMenuState" << endl;
					changeState(gameStateManager, cotw::State::INGAME);
				}
				break;
					
				case sf::Keyboard::Q:
				{
					std::cout << "Bye." << std::endl;
					window->close();
				}
				break;

				default:
					//std::cout << "Unhandled key." << std::endl;
				break;
			}
			break;

		case sf::Event::MouseButtonPressed:
			clickManager.handleClick(sf::Vector2i(event.mouseButton.x, event.mouseButton.y));
			break;

			default:
				// nothing
				break;
		}
	}
}


void MainMenuState::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{

	render_target.draw(background, render_states);

	for (unsigned int i = 0; i < uiElements.size(); i++)
		static_cast<cotw::Button*>(uiElements[i])->draw(render_target, render_states);

	drawingManager.draw(render_target, render_states);
}

void MainMenuState::exitGame()
{
	window->close();
}

}
