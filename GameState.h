#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <SFML/Graphics.hpp>
#include "TextureManager.h"
#include "Drawable.h"

namespace cotw {

class GameStateManager;

enum State { MAINMENU, INGAME };

class GameState: public cotw::Drawable
{
	public:
		cotw::GameStateManager* gameStateManager;
		cotw::TextureManager textureManager;

		virtual void update() = 0;
		virtual void draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const override = 0;
		virtual void changeState(cotw::GameStateManager*, cotw::State) = 0;
		virtual void exitGame() = 0;
	protected:
		virtual void handleKey(sf::RenderWindow*, sf::Event) = 0;
};

}

#endif
