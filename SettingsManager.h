#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

namespace cotw {

class SettingsManager
{
	public:
		SettingsManager();
		~SettingsManager();
};

}

#endif
class Singleton
{
private:
   Singleton();

public:
   static Singleton& instance()
   {
      static Singleton INSTANCE;
      return INSTANCE;
   }
};

class SettingsManager
{
   public:
       static Singleton* getInstance( );
       ~Singleton( );
   private:
       Singleton( );
       static Singleton* instance;
};
