#ifndef BUTTONPAUSE_H
#define BUTTONPAUSE_H

#include "Button.h"
#include "InGameState.h"

namespace cotw {

class ButtonPause: public Button
{
	public:
		ButtonPause(sf::Texture&, std::string, sf::Vector2f, unsigned int, unsigned int);
		~ButtonPause();

		void update();
		void draw(sf::RenderTarget&, sf::RenderStates) const;
		void onClick(cotw::GameState*) override;
};

}

#endif
