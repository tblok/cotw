#ifndef BUTTONQUIT_H
#define BUTTONQUIT_H

#include "Button.h"

namespace cotw {

class ButtonQuit: public Button
{
	public:
		ButtonQuit(sf::Texture&, std::string, sf::Vector2f, unsigned int, unsigned int);
		~ButtonQuit();

		void update();
		void draw(sf::RenderTarget&, sf::RenderStates) const;
		void onClick(cotw::GameState*) override;
};

}

#endif
