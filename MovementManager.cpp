#include <iostream>
#include "MovementManager.h"

using namespace std;

namespace cotw {

MovementManager::MovementManager() 
{

}

MovementManager::~MovementManager()
{

}

bool InGameState::validMove(sf::Vector2<unsigned int> new_pos)
{
	// Wall phasing super power on
	// ---
	//return true;
	// ---

	if (debugPreventCollision)
		return true;

	if ((new_pos.x < 0) || (new_pos.y < 0) || (new_pos.x >= maps[level]->tiles.size()) || (new_pos.y >= maps[level]->tiles.size()))
		return false;
	else if (static_cast<cotw::Tile*>(maps[level]->tiles[new_pos.y][new_pos.x])->blocking)
		return false;
	else
		return true;
}

void InGameState::teleportPlayer(int destRow, int destCol)
{
	int x = 0;
	int y = 0;

	if (player->row > destRow) // if clicked row is above player row
		x = (player->row - destRow) * 32;
	else if (player->row < destRow) // if clicked row is below player row
		x = 0 - ((destRow - player->row) * 32);

	if (player->col > destCol) // if player col is to the left of clicked row
		y = (player->col - destCol) * 32;
	else if (player->col < destCol) // if player col is to the right of clicked row
		y = 0 - ((destCol - player->col) * 32);

	player->col = destCol;
	player->row = destRow;
}

}
