#ifndef POS_H
#define POS_H

namespace cotw {

struct Pos {
	int row;
	int col;
};

}

#endif
