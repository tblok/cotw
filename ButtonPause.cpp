#include <iostream>
#include "ButtonPause.h"

using namespace std;

namespace cotw {

ButtonPause::ButtonPause(sf::Texture& _texture, std::string _text, sf::Vector2f coords, unsigned int _width, unsigned int _height) 
{
	texture = _texture;
	name = "Pause";

	sprite.setTexture(texture);
	sprite.setPosition(coords); 
}

ButtonPause::~ButtonPause()
{

}

void ButtonPause::update()
{

}

void ButtonPause::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{
	render_target.draw(sprite, render_states);
	render_target.draw(text, render_states);
}

void ButtonPause::onClick(cotw::GameState* current_game_state)
{
	cotw::InGameState *current_ingame_state;
	current_ingame_state = static_cast<cotw::InGameState*>(current_game_state);

	if (current_ingame_state->screenPause->visible)
		current_ingame_state->screenPause->visible = false;
	else
		current_ingame_state->screenPause->visible = true;
}

}
