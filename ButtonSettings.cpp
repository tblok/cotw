#include <iostream>
#include "ButtonSettings.h"

using namespace std;

namespace cotw {

ButtonSettings::ButtonSettings(sf::Texture& _texture, std::string _text, sf::Vector2f coords, unsigned int _width, unsigned int _height) 
{
	texture = _texture;
	name = "Settings";

	font.loadFromFile("res/8bit.ttf");
	text.setFont(font);
	text.setCharacterSize(30);
	text.setStyle(sf::Text::Bold);
	text.setFillColor(sf::Color::White);
	text.setString(_text);

	sprite.setTexture(texture);
	sprite.setPosition(coords); 

	center_text();
}

ButtonSettings::~ButtonSettings(){}

void ButtonSettings::update()
{

}

void ButtonSettings::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{
	render_target.draw(sprite, render_states);
	render_target.draw(text, render_states);
}

void ButtonSettings::onClick(cotw::GameState* current_game_state)
{

}

}

