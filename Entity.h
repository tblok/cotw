#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>

namespace cotw {

class Entity
{
	public:
		Entity() {};
		virtual ~Entity() {};

		sf::Sprite sprite;
		sf::Texture texture;

};

}

#endif
