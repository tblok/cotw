#include <iostream>
#include "ButtonStart.h"

using namespace std;

namespace cotw {

ButtonStart::ButtonStart(sf::Texture& _texture, std::string _text, sf::Vector2f coords, unsigned int _width, unsigned int _height) 
{
	texture = _texture;
	name = "Start";

	font.loadFromFile("res/8bit.ttf");
	text.setFont(font);
	text.setCharacterSize(30);
	text.setStyle(sf::Text::Bold);
	text.setFillColor(sf::Color::White);
	text.setString(_text);
	//sf::Vector2f button_pos = coords;
	//button_pos.y += ((height / 2) - (text.getCharacterSize() / 2));
	//cout << button_pos.y << endl;

	sprite.setTexture(texture);
	sprite.setPosition(coords); 
	center_text();

	//sf::Vector2f new_pos = TextManager::GetTextCenterPosition(sprite.getPosition(), texture.getSize(), text.getLocalBounds().width);
	//text.setPosition(new_pos);
}

ButtonStart::~ButtonStart()
{

}

void ButtonStart::update()
{

}

void ButtonStart::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{
	render_target.draw(sprite, render_states);
	render_target.draw(text, render_states);
}

void ButtonStart::onClick(cotw::GameState* current_game_state)
{
	// NEEDS A DELAY TO BE VISIBLE!!!
	//sf::Texture _texture;
	//sf::Image image;
	//image.loadFromFile("textures/gui/button3_pressed.png");

	//if (!_texture.loadFromImage(image))
	//	cout << "texture was not found!" << endl; 

	//texture = _texture;

	current_game_state->changeState(current_game_state->gameStateManager, cotw::State::INGAME);
}

}
