#include "Game.h"

using namespace std;

namespace cotw {

extern int windowSizeH;
extern int windowSizeV;

Game::Game()
{
	sf::VideoMode video_mode(windowSizeH, windowSizeV, 32);
	window.create(video_mode, "Castle of the winds 2018", sf::Style::Titlebar); 
	window.setPosition(sf::Vector2i(464, 50));
	//sf::Image icon;
	//icon.loadFromFile("textures/entity_hero.png");
	//window.setIcon(32, 32, icon.getPixelsPtr());
	gameStateManager = new cotw::GameStateManager(&window);
	gameLoop();
}

Game::~Game()
{
	delete gameStateManager;
}

void Game::update() 
{
	gameStateManager->update();
}

void Game::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) //const
{
	sf::RenderTexture& bitmap = static_cast<sf::RenderTexture&>(render_target);

	bitmap.clear(sf::Color::Black);
	gameStateManager->draw(render_target, render_states);
	bitmap.display();

	// Clear(delete) the contents of the previous frame.
	window.clear();
	window.draw(sf::Sprite(bitmap.getTexture()));
	// Display what has been drawn to the window so far.
	window.display();
}

void Game::gameLoop()
{
	sf::RenderTexture bitmap;
	sf::Vector2u window_size = window.getSize();

	if (!bitmap.create(window_size.x, window_size.y))
	{
		cout << "COTW error: Could not create bitmap" << endl;
		return;
	}

    while (window.isOpen())
    {
		update();
		sf::RenderStates render_state;
		draw(bitmap, render_state);
    }
}


}
