#ifndef GAME_H
#define GAME_H

#include <iostream>
#include "GameStateManager.h"
#include "JsonReader.h"

namespace cotw {

//enum class game_state { GAME, GAME_SETUP, MAIN_MENU };

class Game
{

	public:
		Game();
		~Game();

	private:
        sf::RenderWindow window;
		cotw::GameStateManager *gameStateManager;

		void gameLoop();
		bool validMove(sf::Vector2<unsigned int>);
		void draw(sf::RenderTarget&, sf::RenderStates render_states) ;//const;
		void update();
};

}
#endif
