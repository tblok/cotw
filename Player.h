#ifndef PLAYER_H
#define PLAYER_H

#include "Drawable.h"
#include "Item.h"
#include <unordered_map>
#include "Pos.h"

namespace cotw {

class Player: public cotw::Drawable
{
	public:
		Player();
		Player(sf::Texture&, cotw::Pos);
		~Player();

		std::vector<cotw::Item*> inventory;
		int speed = 32;
		int row;
		int col;

		void update();
		void setDirection(cotw::Direction);
		void move(sf::Vector2i);
};

}

#endif
