#include <algorithm> 

namespace cotw {

/*
	E.g. (0, 32) will produce 0 as the lowest and 31 as the highest value.
*/
int getRandomIntegerBetween(int minValue, int maxValue)
{
	return rand() % (maxValue - minValue + 1) + minValue;
}

}
