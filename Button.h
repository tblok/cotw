#ifndef BUTTON_H
#define BUTTON_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Drawable.h"
#include "GameState.h"
#include "IClickable.h"

namespace cotw {

class Button: public cotw::Drawable, public cotw::IClickable
{
	public:
		sf::Text text;
		sf::Font font;
		//sf::Rect hitbox;

		virtual void update() = 0;
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const = 0;
		virtual void onClick(cotw::GameState*) = 0;

	protected:
		virtual void center_text() { 
			sf::Vector2f text_pos;
			float middle_of_button_x = sprite.getPosition().x + (texture.getSize().x / 2);
			float text_width = text.getLocalBounds().width;

			text_pos.y = (sprite.getPosition().y + (texture.getSize().y / 3));
			text_pos.x = (middle_of_button_x - (text_width / 2));

			text.setPosition(text_pos);
		};
};

}

#endif
