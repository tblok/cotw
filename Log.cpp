#include <iostream>
#include "Log.h"

using namespace std;

namespace cotw {

Log::Log(cotw::Pos pos, std::string base_texture) 
{
	std::string LogW;
	std::string LogE;
	cout << "pos.row = " << pos.row << ", pos.col = " << pos.col << endl;
	boundingBox = new sf::IntRect(pos.row, pos.col, 2, 1);

	const int color = rand() % (1 - 0 + 1) + 0;

	if (color == 1)
	{
		LogW = "LogW1";
		LogE = "LogE1";
	}
	else
	{
		LogW = "LogW2";
		LogE = "LogE2";
	}

	tiles =
	{ 
		{
			new cotw::Tile(textureManager.getMergedTexture(base_texture, LogW), pos.row * 32, pos.col * 32, true),
			new cotw::Tile(textureManager.getMergedTexture(base_texture, LogE), pos.row * 32, ((pos.col + 1) * 32), true)
		}
	};
}

Log::~Log()
{

}

}
