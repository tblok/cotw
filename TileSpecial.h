#ifndef TILESPECIAL_H
#define TILESPECIAL_H

#include <SFML/Graphics.hpp>
#include "TextureManager.h"
#include "Tile.h"

namespace cotw {

class TileSpecial: public cotw::Tile
{
	public:
		TileSpecial(std::string, sf::Texture& _texture, int _x, int _y, bool _blocking);
		~TileSpecial();

	private:
		cotw::TextureManager textureManager;
		sf::Texture *textures;
		sf::Sprite** sprites;
};

}

#endif
