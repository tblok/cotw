#include <iostream>
#include "TextureManager.h"

using namespace std;

namespace cotw {


TextureManager::TextureManager()
{
	loadAllTextures();
}

TextureManager::~TextureManager(){}

void TextureManager::loadAllTextures()
{
	JsonReader json_reader;
	Json::Value tilesheet = json_reader.getJsonData("assets/tilesheet.json");
	sf::Image atlas;
	atlas.loadFromFile("assets/CotwTilesheet.png");

	for (unsigned int i = 0; i < tilesheet.size(); i++)
	{
		// Skip the empty objects in the json file.
		if (!tilesheet[i]["name"].asString().empty())
		{
			const sf::IntRect rect(tilesheet[i]["x"].asInt(), tilesheet[i]["y"].asInt(), 32, 32);
			sf::Texture texture;

			//cout << "Loading \"" + tilesheet[i]["name"].asString() + "\"..." << endl; 
			if (!texture.loadFromImage(atlas, rect))
				cout << "texture \"" + tilesheet[i]["name"].asString() + "\" was not found!" << endl; 

			textures[tilesheet[i]["name"].asString()] = texture;
		}
	}
}


sf::Texture& TextureManager::getTexture(std::string filename, cotw::Atlas atlas)
{
	if (atlas == cotw::Atlas::ui)
	{
		filename = "assets/" + filename;
		std::unordered_map<std::string, sf::Texture>::iterator got = textures.find (filename);

		if (got == textures.end())
		{
			sf::Texture texture;
			sf::Image image;
			image.loadFromFile(filename);

			if (!texture.loadFromImage(image))
				cout << "texture \"" + filename + "\" was not found!" << endl; 

			textures[filename] = texture;
		} 
		//else 
		//{
		//	cout << filename << " was not found in the atlas!" << endl;
		//}
	}
	return textures[filename];
}

sf::Texture& TextureManager::getMergedTexture(std::string base_tex_name1, std::string overlay_tex_name2)
{
	sf::Texture base_tex1 = getTexture(base_tex_name1, cotw::Atlas::game);
	sf::Texture overlay_tex2 = getTexture(overlay_tex_name2, cotw::Atlas::game);
	std::string filename = "Merged" + base_tex_name1 + overlay_tex_name2;

	textures[filename] = mergeTexture(base_tex1, overlay_tex2);

	return textures[filename];
}

sf::Texture TextureManager::mergeTexture(sf::Texture base_texture, sf::Texture overlayTexture)
{
	sf::Texture merged_texture;
	sf::Image base_img = base_texture.copyToImage();
	sf::Image overlay_img = overlayTexture.copyToImage();

	// If IntRect is empty it copies the entire image
	sf::IntRect a;
	base_img.copy(overlay_img, 0, 0, a, true);

	if (!merged_texture.loadFromImage(base_img))
		cout << "texture was not found!" << endl; 
	
	return merged_texture;
}

}
