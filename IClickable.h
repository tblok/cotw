#ifndef ICLICKABLE_H
#define ICLICKABLE_H

namespace cotw {

class GameState;

class IClickable
{
	public:
		virtual ~IClickable() {};
		virtual void onClick(cotw::GameState*) = 0;
};

}

#endif
