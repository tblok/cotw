BUILDDIR = build
CPPFLAGS = -c -std=c++11 -Wall -pedantic
SFMLFLAGS = -lsfml-graphics -lsfml-window -lsfml-system
OBJFILES = Main.o Game.o OverworldMap.o DungeonMap.o Player.o Enemy.o Tile.o TextureManager.o Item.o Console.o MainMenuState.o GameStateManager.o InGameState.o ButtonStart.o ButtonSettings.o ButtonQuit.o ButtonInventory.o ScreenInventory.o ScreenPause.o ButtonPause.o JsonReader.o Tileset.o DrawingManager.o ExperimentalButton.o ClickManager.o OverworldRandomTileFactory.o DungeonRandomTileFactory.o

cotw: $(OBJFILES)
	g++ $(OBJFILES) -o $@ $(SFMLFLAGS) $(UTFLAGS) -ljsoncpp

Main.o: Main.cpp 
	$(CC) $(CPPFLAGS) Main.cpp 

Game.o: Game.cpp 
	$(CC) $(CPPFLAGS) Game.cpp 

OverworldMap.o: OverworldMap.cpp 
	$(CC) $(CPPFLAGS) OverworldMap.cpp 

DungeonMap.o: DungeonMap.cpp 
	$(CC) $(CPPFLAGS) DungeonMap.cpp 

Player.o: Player.cpp 
	$(CC) $(CPPFLAGS) Player.cpp 

Enemy.o: Enemy.cpp 
	$(CC) $(CPPFLAGS) Enemy.cpp 

Tile.o: Tile.cpp 
	$(CC) $(CPPFLAGS) Tile.cpp 

TextureManager.o: TextureManager.cpp 
	$(CC) $(CPPFLAGS) TextureManager.cpp 

Item.o: Item.cpp 
	$(CC) $(CPPFLAGS) Item.cpp 

Console.o: Console.cpp
	$(CC) $(CPPFLAGS) Console.cpp

ButtonStart.o: ButtonStart.cpp
	$(CC) $(CPPFLAGS) ButtonStart.cpp

ButtonInventory.o: ButtonInventory.cpp
	$(CC) $(CPPFLAGS) ButtonInventory.cpp

ButtonSettings.o: ButtonSettings.cpp
	$(CC) $(CPPFLAGS) ButtonSettings.cpp

ButtonQuit.o: ButtonQuit.cpp
	$(CC) $(CPPFLAGS) ButtonQuit.cpp

Button.o: Button.cpp
	$(CC) $(CPPFLAGS) Button.cpp

MainMenuState.o: MainMenuState.cpp
	$(CC) $(CPPFLAGS) MainMenuState.cpp

InGameState.o: InGameState.cpp
	$(CC) $(CPPFLAGS) InGameState.cpp

GameStateManager.o: GameStateManager.cpp
	$(CC) $(CPPFLAGS) GameStateManager.cpp

ScreenInventory.o: ScreenInventory.cpp
	$(CC) $(CPPFLAGS) ScreenInventory.cpp

ScreenPause.o: ScreenPause.cpp
	$(CC) $(CPPFLAGS) ScreenPause.cpp

ButtonPause.o: ButtonPause.cpp
	$(CC) $(CPPFLAGS) ButtonPause.cpp

JsonReader.o: JsonReader.cpp
	$(CC) $(CPPFLAGS) JsonReader.cpp

#Log.o: Log.cpp
#	$(CC) $(CPPFLAGS) Log.cpp

Tileset.o: Tileset.cpp
	$(CC) $(CPPFLAGS) Tileset.cpp

DrawingManager.o: DrawingManager.cpp
	$(CC) $(CPPFLAGS) DrawingManager.cpp

ExperimentalButton.o: ExperimentalButton.cpp
	$(CC) $(CPPFLAGS) ExperimentalButton.cpp

ClickManager.o: ClickManager.cpp
	$(CC) $(CPPFLAGS) ClickManager.cpp

OverworldRandomTileFactory.o: OverworldRandomTileFactory.cpp
	$(CC) $(CPPFLAGS) OverworldRandomTileFactory.cpp

DungeonRandomTileFactory.o: DungeonRandomTileFactory.cpp
	$(CC) $(CPPFLAGS) DungeonRandomTileFactory.cpp

clean:
	rm *.o cotw 
