#ifndef MAP_H
#define MAP_H

#include <algorithm> 
#include <vector>
#include <SFML/Graphics.hpp>
#include "TextureManager.h"
#include "Tile.h"
#include "Tileset.h"
#include "Log.h"
#include "Item.h"
#include "JsonReader.h"
#include "TileFactory.h"
#include "OverworldRandomTileFactory.h"
#include "DungeonRandomTileFactory.h"

//extern bool isEmptyMapEnabled;

namespace cotw {

enum MapType { OUTSIDE, LV1DUNG, LV2DUNG, LV3DUNG };

//extern int isEmptyMapEnabled;
int getRandomIntegerBetween(int, int);

class Map
{
	public:
		// Access with [row][column]
		std::array<std::array<cotw::Drawable*, 31>, 31> tiles;

		virtual void generate() = 0;
	protected:
		cotw::TileFactory* tileFactory;

		int level;
		std::map<std::string, float> tilesetSpawnrates;
		cotw::TextureManager textureManager;

		/**
		 * Unpack the chosen tileset unto the map. 
		 * First loop: without addition from the loop itself, the first tile from the set gets placed in the original
		 * position. Then with help of the incrementing loop values, the selection of the spot on the map where the
		 * the tile must be placed on gets moved.
		 */
		void placeTiles(const std::vector<std::vector<cotw::Tile*>>& tileset, const unsigned start_row, const unsigned int start_col) 
		{
			for (unsigned int tile_row = 0; tile_row < tileset.size(); tile_row++) 
				for (unsigned int tile_col = 0; tile_col < tileset[tile_row].size(); tile_col++) 
					tiles[start_row + tile_row][start_col + tile_col] = tileset.at(tile_row).at(tile_col);
		}

		/**
		 * Places one DungeonStairsDown and/or one DungeonStairsUp tile on each level of the map.
		 * Must later be placed in a method that can "change" more tiles to create unique tiles.
		 */
		void createUniqueTile(std::string tile_type, std::string tileToReplace) 
		{
			bool unsuitableSpawn = true;

			unsigned int spawnableRow;
			unsigned int spawnableCol;

			while (unsuitableSpawn) 
			{
				spawnableRow = getRandomIntegerBetween(0, 30);
				spawnableCol = getRandomIntegerBetween(0, 30);
				
				if (static_cast<cotw::Tile*>(tiles[spawnableRow][spawnableCol])->name == tileToReplace)
					unsuitableSpawn = static_cast<cotw::Tile*>(tiles[spawnableRow][spawnableCol])->blocking;
			}

			static_cast<cotw::Tile*>(tiles[spawnableRow][spawnableCol])->originalTexture = textureManager.getTexture(tile_type, cotw::Atlas::game);
			static_cast<cotw::Tile*>(tiles[spawnableRow][spawnableCol])->sprite.setTexture(static_cast<cotw::Tile*>(tiles[spawnableRow][spawnableCol])->originalTexture);
			static_cast<cotw::Tile*>(tiles[spawnableRow][spawnableCol])->name = tile_type;
			static_cast<cotw::Tile*>(tiles[spawnableRow][spawnableCol])->blocking = 0;

			std::cout << "Placed " << tile_type << " at row = " << spawnableRow << " and col = " << spawnableCol << std::endl;
		}
};

}

#endif
