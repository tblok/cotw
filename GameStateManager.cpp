#include <iostream>
#include "GameStateManager.h"

using namespace std;

namespace cotw {

GameStateManager::GameStateManager(sf::RenderWindow *render_window)
{ 
	//states[0] = new cotw::MainMenuState(this, render_window);
	states[0] = new cotw::InGameState(this, render_window);
	current_state = states[0];
}

GameStateManager::~GameStateManager(){ }

void GameStateManager::setState(cotw::State state)
{
	current_state = states[state];
}

void GameStateManager::update()
{
	current_state->update();
}

void GameStateManager::draw(sf::RenderTarget& render_target, sf::RenderStates render_states)
{
	current_state->draw(render_target, render_states);
}

}
