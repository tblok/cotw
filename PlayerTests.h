#ifndef MISC_H
#define MISC_H

#include <cppunit/extensions/HelperMacros.h>
#include <SFML/Graphics.hpp>
//#include "Game.h"
#include "Map.h"
#include "Player.h"
#include "Tile.h"

class PlayerTests: public CPPUNIT_NS::TestFixture 
{
	CPPUNIT_TEST_SUITE( PlayerTests ); // Note 3 

	CPPUNIT_TEST( collision );

	CPPUNIT_TEST_SUITE_END();

	public:
		void setUp();
		void tearDown();

	protected:

	private:
		//cotw::Game game;
		cotw::Player *player;
		cotw::Map map;
		void player_exists();
		void collision();
		void swap_tile(sf::Vector2f, cotw::Tile*);
};

#endif
