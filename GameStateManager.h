#ifndef GAMESTATEMANAGER_H
#define GAMESTATEMANAGER_H

#include "GameState.h"
#include "InGameState.h"
#include "MainMenuState.h"

namespace cotw {


class GameStateManager
{
	public:
		GameStateManager(sf::RenderWindow*);
		~GameStateManager();

		void update();
		void draw(sf::RenderTarget&, sf::RenderStates);
		void setState(cotw::State);

	private:
		std::array<cotw::GameState*, 1> states;
		cotw::GameState *current_state;

};

}

#endif
