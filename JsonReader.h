#ifndef JSONREADER_H
#define JSONREADER_H

#include <jsoncpp/json/json.h>
#include <fstream>

namespace cotw {

class JsonReader
{
	public:
		JsonReader();
		~JsonReader();

		Json::Reader reader;
	    Json::Value getJsonData(std::string);
};

}

#endif
