#ifndef SCREENINVENTORY_H
#define SCREENINVENTORY_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Drawable.h"
#include "GameState.h"
#include "IClickable.h"

namespace cotw {

class ScreenInventory: public cotw::Drawable, public cotw::IClickable
{
	public:
		ScreenInventory(sf::Texture&, std::string, sf::Vector2f);
		~ScreenInventory();
		bool visible;

		void update();
		void draw(sf::RenderTarget&, sf::RenderStates) const;
		void onClick(cotw::GameState*);
};

}

#endif
