#ifndef LOG_H
#define LOG_H

#include "Tileset.h"

namespace cotw {

class Log: public Tileset
{
	public:
		Log(cotw::Pos, std::string);
		~Log();
};

}

#endif
