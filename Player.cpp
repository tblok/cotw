#include <iostream>
#include "Player.h"

using namespace std;

namespace cotw {

Player::Player() {}

Player::Player(sf::Texture& _texture, cotw::Pos pos) 
{
	texture = _texture;
	row = pos.row;
	col = pos.col;

	//sprite.setPosition(sf::Vector2f(row * 32, col * 32)); 
	sprite.setPosition(col * 32, row * 32); 
	sprite.setTexture(texture);
	// Set first top left
	sprite.setTextureRect(sf::IntRect(32, 0, 32, 32));
	// Set next to first top left
	//sprite.setTextureRect(sf::IntRect(32, 0, 32, 32));
}

Player::~Player(){}

void Player::update() 
{

}

void Player::setDirection(cotw::Direction direction) 
{
	if (direction == cotw::Direction::NORTH)
		sprite.setTextureRect(sf::IntRect(32, 0, 32, 32));
	else if (direction == cotw::Direction::EAST)
		sprite.setTextureRect(sf::IntRect(32, 32, 32, 32));
	else if (direction == cotw::Direction::SOUTH)
		sprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	else if (direction == cotw::Direction::WEST)
		sprite.setTextureRect(sf::IntRect(0, 32, 32, 32));
}

void Player::move(sf::Vector2i new_pos) 
{
//	cout << "data in param: " << new_pos.x << ", " << new_pos.y << endl;
//	cout << "Current row and col: " << row << ", " << col << endl;
	row += new_pos.y;
	col += new_pos.x;
	//cout << "New row and col: " << row << ", " << col << endl;

}

}
