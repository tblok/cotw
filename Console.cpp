#include "Console.h"

using namespace std;

namespace cotw {

Console::Console(sf::Texture& _texture, std::string _text, sf::Vector2f coords/*, unsigned int _width, unsigned int _height*/)
{
	texture = _texture;
	//width = _width;
	//height = _height;
	font.loadFromFile("res/DejaVuSans.ttf");
	sf::Vector2f text_coords = coords;
	text_coords.y += 8;
	text_coords.x += 10;
	name = "Console";


	for (unsigned int i = 0; i < texts.size(); i++)
	{
		texts[i].setFont(font);
		texts[i].setCharacterSize(12);
		texts[i].setStyle(sf::Text::Bold);
		texts[i].setFillColor(sf::Color::White);
		texts[i].setString("");
		if (i != 0)
			text_coords.y += 13;
		texts[i].setPosition(text_coords);
	}

	sprite.setTexture(texture);
	sprite.setPosition(coords); 
}

Console::~Console() {}

void Console::log(std::string msg, sf::Color color) 
{
	texts[0].setFillColor(color);
	messages.insert(messages.begin(), msg);
}

void Console::update() 
{
	// klopt want voor bericht 1 moet er 1 size zijn, voor bericht 2 moet er ten minste 2 size zijn..
	for (unsigned int i = 0; i < texts.size(); i++)
		if (messages.size() >= (i + 1))
			texts[i].setString(messages.at(i));
}

void Console::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{
	render_target.draw(sprite, render_states);
	for (unsigned int i = 0; i < texts.size(); i++)
		render_target.draw(texts[i], render_states);
}

void Console::onClick(cotw::GameState* current_game_state)
{
	current_game_state->changeState(current_game_state->gameStateManager, cotw::State::MAINMENU);
}

}
