#ifndef DUNGEONMAP_H
#define DUNGEONMAP_H

#include <algorithm> 
#include <vector>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <array>
#include "Map.h"

namespace cotw {

class DungeonMap: public cotw::Map
{
	public:
		DungeonMap();
		~DungeonMap();

		void generate();
	private:
		std::array<sf::IntRect, 10> rooms;

		void createRooms();
		void createRoom(sf::IntRect, int);
		void overlaySpecialTiles();
		void digTunnels();
		void createTunnel(sf::IntRect);
		void createHTunnel(int, int, int);
		void createVTunnel(int, int, int);
		const std::vector<sf::IntRect> getUnconnectedRooms();
};

}

#endif
