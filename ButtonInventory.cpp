#include <iostream>
#include "ButtonInventory.h"

using namespace std;

namespace cotw {

ButtonInventory::ButtonInventory(sf::Texture& _texture, std::string _text, sf::Vector2f coords, unsigned int _width, unsigned int _height) 
{
	texture = _texture;
	name = "Inventory";

	//font.loadFromFile("DejaVuSans.ttf");
	//text.setFont(font);
	//text.setCharacterSize(50);
	//text.setStyle(sf::Text::Bold);
	//text.setColor(sf::Color::White);
	//text.setString(_text);

	sprite.setTexture(texture);
	sprite.setPosition(coords); 

	//sf::Vector2f dimensions = sprite.getPosition();
	//sf::Vector2f text_coords = coords;
	//text_coords.y += ((dimensions.y / 2) - dimensions.y / 3);
	//text_coords.x += 60;
	//text.setPosition(text_coords);
}

ButtonInventory::~ButtonInventory()
{

}

void ButtonInventory::update()
{

}

void ButtonInventory::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{
	render_target.draw(sprite, render_states);
	render_target.draw(text, render_states);
}

void ButtonInventory::onClick(cotw::GameState* current_game_state)
{
	cotw::InGameState *current_ingame_state;
	current_ingame_state = static_cast<cotw::InGameState*>(current_game_state);

	if (current_ingame_state->screenInventory->visible)
		current_ingame_state->screenInventory->visible = false;
	else
		current_ingame_state->screenInventory->visible = true;
}

}
