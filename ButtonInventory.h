#ifndef BUTTONINVENTORY_H
#define BUTTONINVENTORY_H

#include "Button.h"
#include "InGameState.h"

namespace cotw {

class ButtonInventory: public Button
{
	public:
		ButtonInventory(sf::Texture&, std::string, sf::Vector2f, unsigned int, unsigned int);
		~ButtonInventory();

		void update();
		void draw(sf::RenderTarget&, sf::RenderStates) const;
		void onClick(cotw::GameState*) override;
};

}

#endif
