// _x = row and _y = col
#include "Tile.h"

using namespace std;

namespace cotw {

Tile::Tile(){}

// _x = row and _y = col
Tile::Tile(std::string _name, sf::Texture& _texture, int col, int row, bool _blocking)
//Tile::Tile(sf::Image& _img, int _x, int _y, bool _blocking)
{
	name = _name;
	texture = _texture;
	originalTexture = texture;
	blocking = _blocking;
	sprite.setTexture(texture);
	sprite.setPosition(sf::Vector2f(row, col)); 
}


Tile::~Tile(){}

void Tile::update() {}

void Tile::conceal()
{
	//sf::Image fogImg;
	//fogImg.create(32, 32, sf::Color::Red);
	//sf::Texture fogTex;
	//if (!fogTex.loadFromImage(fogImg))
	//	cout << "texture was not found!" << endl; 

	//sprite.setTexture(fogTex);
	sprite.setColor(sf::Color(255, 255, 255, 0)); // half transparent
}

void Tile::reveal()
{
	//sprite.setTexture(originalTexture);
	sprite.setColor(sf::Color(255, 255, 255, 255)); // half transparent
}

void Tile::addToInventory(cotw::Item* item)
{
	inventory.push_back(item);
	overlayTexture(item->image);
}

void Tile::removeFromInventory()
{
	inventory.erase(inventory.begin());
	overlayTexture(originalTexture.copyToImage());
}

// Add to inv... also sets image

void Tile::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const 
{ 
	render_target.draw(sprite, render_states); 
}

void Tile::overlayTexture(const sf::Image& new_img)
{
	sf::Image curr_img = texture.copyToImage();
	sf::IntRect a;
	curr_img.copy(new_img, 0, 0, a, true);

	sf::Texture new_tex;

	if (!new_tex.loadFromImage(curr_img))
		cout << "texture was not found!" << endl; 

	texture = new_tex;
}

void Tile::interact()
{
	//cout << "a" << endl;
	//cout << "a" << endl;

}

// Col to the right is (1, 0) and what is received here.
//void Tile::move(sf::Vector2i new_pos)
//{
//	// second is col
//	sprite.move(new_pos.x * 32, new_pos.y * 32);
//}

}
