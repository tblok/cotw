#include "DrawingManager.h"

namespace cotw {

DrawingManager::DrawingManager() {}

DrawingManager::~DrawingManager() {}

void DrawingManager::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{
	for (auto entity : drawables)
		render_target.draw(entity->sprite, render_states);
	//render_target.draw(text, render_states);
}

// Add a "add" method. Because what if we change the container in the future? If we had an add method we only have to change it here and not everywhere throughout the program.

}
