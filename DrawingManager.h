#ifndef DRAWINGMANAGER_H
#define DRAWINGMANAGER_H

#include <vector>
#include "Entity.h"
#include <SFML/Graphics.hpp>

namespace cotw {

class DrawingManager
{
	public:
		DrawingManager();
		~DrawingManager();

		std::vector<Entity*> drawables;
		void draw(sf::RenderTarget&, sf::RenderStates) const;
};

}

#endif
