#ifndef INGAMESTATE_H
#define INGAMESTATE_H

#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>
#include "GameState.h"
#include "Drawable.h"
#include "Button.h"
#include "ButtonInventory.h"
#include "ButtonPause.h"
#include "Console.h"
#include "ScreenInventory.h"
#include "ScreenPause.h"
#include "IClickable.h"
#include "MainMenuState.h"
#include "Map.h"
#include "OverworldMap.h"
#include "DungeonMap.h"
#include "Player.h"
#include "Enemy.h"
#include "Tile.h"
#include "Pos.h"

namespace cotw {

class InGameState: public cotw::GameState
{
	public:
		InGameState(cotw::GameStateManager*, sf::RenderWindow*);
		~InGameState();

		sf::RenderWindow *window;
		std::array<cotw::Map*, 4> maps;
		cotw::Console *console;
		cotw::ScreenInventory *screenInventory;
		cotw::ScreenPause *screenPause;
		std::array<cotw::Drawable*, 5> uiElements;
		cotw::GameStateManager* gameStateManager;
		std::string foo = "foo";
		cotw::Player *player;
		cotw::Enemy *enemy1;
		cotw::Enemy *enemy2;
		cotw::Enemy *enemy3;

		void draw(sf::RenderTarget&, sf::RenderStates) const;
		void reorientViewport(sf::Vector2f);
		void changeState(cotw::GameStateManager*, cotw::State);
		void exitGame();
		void update();
		int level = 0;
		void revealLos();
		bool validMove(sf::Vector2<unsigned int>);
		void teleportPlayer(int, int);
		void handleKey(sf::RenderWindow*, sf::Event);
	private:
		void setupUi();
		std::string helpMessage = "[1]: Cycle between levels [2] Print tile information under player [3] Reveal all hidden tiles [4] Print enemy info. [5] Activate noclip.";
		bool noclip = false;
		void enterDungeon(bool);
		void spawnPlayerOnDungeonEntrance();
};

}

#endif
