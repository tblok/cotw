#ifndef MAINMENUSTATE_H
#define MAINMENUSTATE_H

#include "GameState.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Drawable.h"
#include "Button.h"
#include "ButtonStart.h"
#include "ButtonSettings.h"
#include "ButtonQuit.h"
#include "InGameState.h"
#include "DrawingManager.h"
#include "ExperimentalButton.h"
#include "ClickManager.h"
#include "Entity.h"
//#include "GameStateManager.h"

namespace cotw {

class MainMenuState: public cotw::GameState
{
	public:
		MainMenuState(cotw::GameStateManager*, sf::RenderWindow*);
		~MainMenuState();

		sf::RenderWindow *window;
		std::string foo = "foo";
		std::array<cotw::Drawable*, 2> uiElements;
		sf::Sprite background;
		cotw::DrawingManager drawingManager;
		cotw::ClickManager clickManager;

		void callback();
		void setupUi();
		void draw(sf::RenderTarget&, sf::RenderStates) const;
		void update();
		void handleKey(sf::RenderWindow*, sf::Event);
		void changeState(cotw::GameStateManager*, cotw::State);
		void exitGame();
};

}

#endif
