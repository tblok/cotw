#include <iostream>
#include "JsonReader.h"

using namespace std;

namespace cotw {

JsonReader::JsonReader() 
{

}

JsonReader::~JsonReader()
{

}

Json::Value JsonReader::getJsonData(std::string filename)
{
	Json::Value read_data;
	std::ifstream ifs(filename);

    reader.parse(ifs, read_data);

	return read_data;
}

}
