#ifndef TILECHEST_H
#define TILECHEST_H

#include "TileSpecial.h"

namespace cotw {

class TileChest: public TileSpecial
{
	public:
		TileChest();
		~TileChest();

		void interact() override;
};

}

#endif
