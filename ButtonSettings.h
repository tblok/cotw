#ifndef BUTTONSETTINGS_H
#define BUTTONSETTINGS_H

#include "Button.h"

namespace cotw {

class ButtonSettings: public Button
{
	public:
		ButtonSettings(sf::Texture&, std::string, sf::Vector2f, unsigned int, unsigned int);
		~ButtonSettings();

		void update();
		void draw(sf::RenderTarget&, sf::RenderStates) const;
		void onClick(cotw::GameState*) override;
};

}

#endif
