#ifndef SCREENPAUSE_H
#define SCREENPAUSE_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Drawable.h"
#include "GameState.h"
#include "TextureManager.h"
#include "IClickable.h"

namespace cotw {

class ScreenPause: public cotw::Drawable, public cotw::IClickable
{
	public:
		ScreenPause(sf::Texture&, std::string, sf::Vector2f);
		~ScreenPause();
		// better to have a pause state actually.. should remove this entire class....
		//cotw::TextureManager textureManager;
		//std::array<cotw::Drawable*, 4> uiElements;
		bool visible;

		void update();
		void draw(sf::RenderTarget&, sf::RenderStates) const;
		void onClick(cotw::GameState*);
};

}

#endif
