#include <iostream>
#include "InGameState.h"
#include "GameStateManager.h"

using namespace std;

namespace cotw {

extern int windowSizeH;
extern int windowSizeV;
extern bool debugPreventCollision;

InGameState::InGameState(cotw::GameStateManager* _gameStateManager, sf::RenderWindow *_window) 
{
	gameStateManager = _gameStateManager;
	window = _window;

	player = new cotw::Player(textureManager.getTexture("entity_hero.png", cotw::Atlas::ui), cotw::Pos {15, 15});
	player->setDirection(cotw::Direction::SOUTH);
	
	level = 0;

	maps[0] = new OverworldMap;
	maps[1] = new DungeonMap;
	maps[2] = new DungeonMap;
	maps[3] = new DungeonMap;

	maps[0]->generate();
	maps[1]->generate();
	maps[2]->generate();
	maps[3]->generate();

	setupUi();

	console->log(("Tap \"h\" for help"), sf::Color::White);
	console->log((helpMessage), sf::Color::White);

	spawnPlayerOnDungeonEntrance();
}

void InGameState::spawnPlayerOnDungeonEntrance() 
{
	for (unsigned int row = 0; row < maps[level]->tiles.size(); row++) 
		for (unsigned int col = 0; col < maps[level]->tiles.size(); col++) 
			if (static_cast<cotw::Tile*>(maps[level]->tiles[row][col])->name == "DungeonEntrance")
			{
				teleportPlayer(row, col);
				row = 30;
				col = 30;
			}
}

InGameState::~InGameState() {}

void InGameState::setupUi()
{
	sf::Texture texScreenConsole(textureManager.getTexture("gui/console3.png", cotw::Atlas::ui));
	sf::Texture texButton = textureManager.getTexture("gui/ButtonInventory4.png", cotw::Atlas::ui);
	sf::Texture texButtonPause = textureManager.getTexture("gui/ButtonPause.png", cotw::Atlas::ui);
	sf::Texture texScreenInventory(textureManager.getTexture("gui/ScreenInventory2.png", cotw::Atlas::ui));
	sf::Texture texScreenPause(textureManager.getTexture("gui/ScreenPause.png", cotw::Atlas::ui));

	console = new cotw::Console
	(
		texScreenConsole, 
		"", 
		sf::Vector2f(0, windowSizeV - texScreenConsole.getSize().y)
	);

	cotw::Button *button_inventory = new cotw::ButtonInventory
	(
		texButton,
		"",
		sf::Vector2f(windowSizeH - texButton.getSize().x, windowSizeV - texButton.getSize().y),
		texButton.getSize().x,
		texButton.getSize().y
	);

	cotw::Button *button_pause = new cotw::ButtonPause
	(
		texButtonPause,
		"",
		sf::Vector2f((windowSizeH - texButtonPause.getSize().x), (windowSizeV - texButtonPause.getSize().y * 2)),
		texButton.getSize().x,
		texButton.getSize().y
	);

	screenInventory = new cotw::ScreenInventory
	(
		texScreenInventory, 
		"", 
		sf::Vector2f(0, (windowSizeV - texScreenInventory.getSize().y) - (texScreenConsole.getSize().y) - 1)
	);

	screenPause = new cotw::ScreenPause
	(
		texScreenPause, 
		"", 
		sf::Vector2f(0, (windowSizeV - texScreenInventory.getSize().y) - (texScreenConsole.getSize().y) - 1)
	);

	uiElements[0] = console;
	uiElements[1] = button_inventory;
	uiElements[2] = button_pause;
	uiElements[3] = screenInventory;
	uiElements[4] = screenPause;
}

bool InGameState::validMove(sf::Vector2<unsigned int> new_pos)
{
	if (debugPreventCollision || noclip)
		return true;

	if ((new_pos.x < 0) || (new_pos.y < 0) || (new_pos.x >= maps[level]->tiles.size()) || (new_pos.y >= maps[level]->tiles.size()))
		return false;
	else if (static_cast<cotw::Tile*>(maps[level]->tiles[new_pos.y][new_pos.x])->blocking)
		return false;
	else
		return true;
}

void InGameState::changeState(cotw::GameStateManager* gameStateManager, cotw::State state)
{
	gameStateManager->setState(state);
}


void InGameState::update() 
{
	sf::Event event;

	while (window->pollEvent(event))
		handleKey(window, event);

	console->update();
}

void InGameState::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{	
	for (int row = (player->row - 15); row < (player->row + 15) + 1; row++)
	{
		for (int col = (player->col - 15); col < (player->col + 15) + 1; col++)
		{
			if ((col >= 0) && (row >= 0) && (col < (int)maps[level]->tiles.size()) && (row < (int)maps[level]->tiles.size()))
			{
				sf::RenderStates render_states;
				maps[level]->tiles[row][col]->draw(render_target, render_states);
			}
		}
	}

	player->draw(render_target, render_states);

	for (unsigned int i = 0; i < uiElements.size(); i++)
		static_cast<cotw::Button*>(uiElements[i])->draw(render_target, render_states);
}

/**
 * Reveals a area of tiles around the player after each move.
 * NOTE: Bugged when approaching edge on outside of map, well it 
 * shouldn't even trigger then but whatever... for now.
 */
void InGameState::revealLos()
{
	int viewDistance = 4;

	int startRectRow = player->row - viewDistance;
	int startRectCol = player->col - viewDistance;
	
	if (startRectRow < 0)
		startRectRow = 0;

	if (startRectCol < 0)
		startRectCol = 0;

	int rowsToCheck = ((viewDistance * 2)+ 1);
	int colsToCheck = ((viewDistance * 2) + 1);

	if (startRectRow + rowsToCheck > 30)
		rowsToCheck = rowsToCheck - ((startRectRow + rowsToCheck) - 32);
	if (startRectCol + colsToCheck > 30)
		colsToCheck = colsToCheck - ((startRectCol + colsToCheck) - 32);

	for (int row = startRectRow; row < startRectRow + rowsToCheck; row++)
		for (int col = startRectCol; col < startRectCol + colsToCheck; col++)
				static_cast<cotw::Tile*>(maps[level]->tiles[row][col])->reveal();
}

void InGameState::handleKey(sf::RenderWindow* window, sf::Event event)
{
	switch (event.type) 
	{
		case sf::Event::KeyPressed:
		{
			switch (event.key.code) 
			{
				case sf::Keyboard::Numpad7:
				{
					if(validMove(sf::Vector2<unsigned int>(player->col - 1, player->row - 1)))
					{
						player->move(sf::Vector2i(-1, -1));
						reorientViewport(sf::Vector2f(32, 32));
					}
				}
				break;

				case sf::Keyboard::Numpad8:
				{
					if(validMove(sf::Vector2<unsigned int>(player->col, player->row - 1)))
					{
						player->move(sf::Vector2i(0, -1));
						reorientViewport(sf::Vector2f(0, 32));
					}
				}
				break;

				case sf::Keyboard::Numpad9:
				{
					if(validMove(sf::Vector2<unsigned int>(player->col + 1, player->row - 1)))
					{
						player->move(sf::Vector2i(1, -1));
						reorientViewport(sf::Vector2f(-32, 32));
					}
				}
				break;

				case sf::Keyboard::Numpad4:
				{
					if(validMove(sf::Vector2<unsigned int>(player->col - 1, player->row)))
					{
						player->move(sf::Vector2i(-1, 0));
						reorientViewport(sf::Vector2f(32, 0));
					}
				}
				break;

				case sf::Keyboard::Numpad6:
				{
					if(validMove(sf::Vector2<unsigned int>(player->col + 1, player->row)))
					{
						player->move(sf::Vector2i(1, 0));
						reorientViewport(sf::Vector2f(-32, 0));
					}
				}
				break;

				case sf::Keyboard::Numpad1:
				{
					if(validMove(sf::Vector2<unsigned int>(player->col - 1, player->row + 1)))
					{
						player->move(sf::Vector2i(-1, 1));
						reorientViewport(sf::Vector2f(32, -32));
					}
				}
				break;

				case sf::Keyboard::Numpad2:
				{
					if(validMove(sf::Vector2<unsigned int>(player->col, player->row + 1)))
					{
						player->move(sf::Vector2i(0, 1));
						reorientViewport(sf::Vector2f(0, -32));
					}
				}
				break;

				case sf::Keyboard::Numpad3:
				{
					if(validMove(sf::Vector2<unsigned int>(player->col + 1, player->row + 1)))
					{
						player->move(sf::Vector2i(1, 1));
						reorientViewport(sf::Vector2f(-32, -32));
					}
				}
				break;

				case sf::Keyboard::Numpad5:
				{
					if (static_cast<cotw::Tile*>(maps[level]->tiles[player->row][player->col])->name == "DungeonEntrance")
					{
						level++;
						enterDungeon(true);
					}
					else if (static_cast<cotw::Tile*>(maps[level]->tiles[player->row][player->col])->name == "DungeonStairsDown")
					{
						if (level == 3)
							level = 0;
						else
							level++;

						int destinationRow;
						int destinationCol;

						for (unsigned int row = 0; row < maps[level]->tiles.size(); row++) 
							for (unsigned int col = 0; col < maps[level]->tiles.size(); col++) 
								if (static_cast<cotw::Tile*>(maps[level]->tiles[row][col])->name == "DungeonStairsUp")
								{
									destinationRow = row;
									destinationCol = col;
									row = 30;
									col = 30;
								}

						teleportPlayer(destinationRow, destinationCol);
					}
					else if (static_cast<cotw::Tile*>(maps[level]->tiles[player->row][player->col])->name == "DungeonStairsUp")
					{
						level--;

						int destinationRow;
						int destinationCol;

						for (unsigned int row = 0; row < maps[level]->tiles.size(); row++) 
							for (unsigned int col = 0; col < maps[level]->tiles.size(); col++) 
								if ((static_cast<cotw::Tile*>(maps[level]->tiles[row][col])->name == "DungeonEntrance") || (static_cast<cotw::Tile*>(maps[level]->tiles[row][col])->name == "DungeonStairsDown"))
								{
									destinationRow = row;
									destinationCol = col;
									row = 30;
									col = 30;
								}

						teleportPlayer(destinationRow, destinationCol);
					}
					else 
					{
						cout << "Loot: ";

						if (static_cast<cotw::Tile*>(maps[level]->tiles[player->row][player->col])->inventory.size() == 0)
						{
							cout << "nothing of interest." << endl;
							console->log(("Nothing to interact with here."), sf::Color::White);
						}
						else
						{
							cotw::Item *p_item = static_cast<cotw::Tile*>(maps[level]->tiles[player->row][player->col])->inventory.front();
							console->log(("You receive item [" + p_item->name + "]."), sf::Color::White);
							player->inventory.push_back(p_item);
							static_cast<cotw::Tile*>(maps[level]->tiles[player->row][player->col])->removeFromInventory();
						}
					}
				}
				break;

				case sf::Keyboard::B:
				{
					if (screenInventory->visible)
						screenInventory->visible = false;
					else
						screenInventory->visible = true;
				}
				break;

				case sf::Keyboard::Escape:
				{
					if (screenPause->visible)
						screenPause->visible = false;
					else
						screenPause->visible = true;
				}
				break;
				
				case sf::Keyboard::Q:
				{
					window->close();
				}
				break;

				case sf::Keyboard::Num1:
				{
					if (level == 3)
						level = 0;
					else
						level++;

					enterDungeon(false);
				}
				break;

				case sf::Keyboard::Num2:
				{
					std::string aaa = "Player at [" + std::to_string(player->row) + "][" + std::to_string(player->col) + "] (row, col). Tile id = " + static_cast<cotw::Tile*>(maps[level]->tiles[player->row][player->col])->name + ", blocking = " + std::to_string(static_cast<cotw::Tile*>(maps[level]->tiles[player->row][player->col])->blocking);
					console->log(aaa, sf::Color::White);
				}
				break;

				case sf::Keyboard::Num3:
				{
					for (unsigned int i = 0; i < 4; i++) 
						for (unsigned int row = 0; row < maps[level]->tiles.size(); row++) 
							for (unsigned int col = 0; col < maps[level]->tiles.size(); col++) 
								static_cast<cotw::Tile*>(maps[i]->tiles[row][col])->reveal();
				}
				break;

				case sf::Keyboard::Num4:
				{
					std::string aaa = "Enemy at [" + std::to_string(enemy1->row) + "][" + std::to_string(enemy1->col) + "] (row, col). Tile id = " + static_cast<cotw::Tile*>(maps[level]->tiles[enemy1->row][enemy1->col])->name;
					console->log(aaa, sf::Color::White);
				}
				break;

				case sf::Keyboard::Num5:
				{
					std::string noclip_activation_message;
					if (noclip)
					{
						noclip = false;
						noclip_activation_message = "Noclip activated";
					}
					else
					{
						noclip = true;
						noclip_activation_message = "Noclip deactivated";
					}

					console->log(noclip_activation_message, sf::Color::White);
				}
				break;

				case sf::Keyboard::H:
				{
					console->log(("Tap \"h\" for help"), sf::Color::White);
					console->log((helpMessage), sf::Color::White);
				}
				break;

				case sf::Keyboard::I:
				{
					// Inventory contents
					// ################################################################################################################
					//std::string aaa = "Inventory contents";
					//console->log(aaa, sf::Color::White);

					//aaa = "--------------";
					//console->log(aaa, sf::Color::White);
					//for (auto item : player->inventory) 
					//{
					//	aaa = item->name;
					//	console->log(aaa, sf::Color::White);
					//}
					//aaa = "--------------";
					//console->log(aaa, sf::Color::White);
					// ################################################################################################################

				}
				break;

				case sf::Keyboard::Return:
				{
					window->close();
				}
				break;

				default:
					cout << "default" << endl;
				break;

			}
			break;


		}
		case sf::Event::MouseButtonPressed:
		{
			// Get texture from the tile clicked on and place it in the center for evaluation
			// ##############################################################################
			//int col = (event.mouseButton.x / 32) + (player->col - 15);
			//int row = (event.mouseButton.y / 32) + (player->row - 15);

			//const sf::Texture *texture = static_cast<cotw::Tile*>(map->tiles[row][col])->sprite.getTexture();

			//sf::Vector2u window_size = window->getSize();
			//int ccol = ((windowSizeH / 2) / 32) - 1;
			//int rrow = ((windowSizeV / 2) / 32);
			//static_cast<cotw::Tile*>(map->tiles[ccol][rrow])->sprite.setTexture(*texture);
			//sf::Vector2f pos = static_cast<cotw::Tile*>(map->tiles[row][col])->sprite.getPosition();
			//cout << "x: " << pos.x << ", y: " << pos.y << endl;
			// ##############################################################################
			
			// Place a dungeon entrance tile where a player clicks
			// ##############################################################################
			//const sf::Texture *texture = new sf::Texture(textureManager.getTexture("DungeonEntrance", cotw::Atlas::game));

			////sf::Vector2u window_size = window->getSize();
			//int col = (event.mouseButton.x / 32) + (player->col - 15);
			//int row = (event.mouseButton.y / 32) + (player->row - 15);
			//static_cast<cotw::Tile*>(maps[level]->tiles[row][col])->sprite.setTexture(*texture);
			// ##############################################################################

			// Teleport player to clicked tile
			// ##############################################################################
			int col = (event.mouseButton.x / 32) + (player->col - 15);
			int row = (event.mouseButton.y / 32) + (player->row - 15);
			teleportPlayer(row, col);
			// ##############################################################################

			//sf::RenderStates render_states;
			//sf::Vector2f old_pos = static_cast<cotw::Tile*>(map->tiles[row][col])->sprite.getPosition();
			//static_cast<cotw::Tile*>(map->tiles[row][col])->sprite.setPosition(sf::Vector2f(event.mouseButton.y, event.mouseButton.x)); 
			//static_cast<cotw::Tile*>(map->tiles[row][col])->draw(render_target, render_states);
			//static_cast<cotw::Tile*>(map->tiles[row][col])->sprite.setPosition(old_pos); 
			//cout << event.mouseButton.x << ", " << event.mouseButton.y << endl;
			//cout << "col: "<< col << ", row: " << row << "." << endl;


			// Ui clicking code
			//for (auto ui_element : uiElements) 
			//{
			//	sf::FloatRect clicked_area(event.mouseButton.x, event.mouseButton.y, 1, 1);
			//	sf::FloatRect rect = ui_element->sprite.getGlobalBounds();
			//	bool clicked = rect.intersects(clicked_area);

			//	if (clicked)
			//	{
			//		cout << "Clicked on " << ui_element->name << endl;
			//		if (cotw::IClickable* clickable = dynamic_cast<cotw::IClickable*>(ui_element))
			//			clickable->onClick(this);
			//	}
			//}
		}
		break;

		default:
		break;

	}
}

void InGameState::teleportPlayer(int destRow, int destCol)
{
	// If clicking somewhere southeast of the most lower right tile
	if (destRow > 30 && destCol > 30)
	{
		destRow = 30;
		destCol = 30;
	}
	// If clicking somewhere northwest of the most upper left tile
	else if (destRow < 0 && destCol < 0)
	{
		destRow = 0;
		destCol = 0;
	}
	// If clicking somewhere northwest of the most upper right tile
	else if (destRow < 0 && destCol > 30)
	{
		destRow = 0;
		destCol = 30;
	}
	// If clicking somewhere southwest of the most lower right tile
	else if (destRow > 30 && destCol < 0)
	{
		destRow = 30;
		destCol = 0;
	}
	else if (destRow > 30)
		destRow = 30;
	else if (destRow < 0)
		destRow = 0;
	else if (destCol > 30)
		destCol = 30;
	else if (destCol < 0)
		destCol = 0;
	
	int x = 0;
	int y = 0;

	if (player->row > destRow) // if clicked row is above player row
		x = (player->row - destRow) * 32;
	else if (player->row < destRow) // if clicked row is below player row
		x = 0 - ((destRow - player->row) * 32);

	if (player->col > destCol) // if player col is to the left of clicked row
		y = (player->col - destCol) * 32;
	else if (player->col < destCol) // if player col is to the right of clicked row
		y = 0 - ((destCol - player->col) * 32);

	player->col = destCol;
	player->row = destRow;

	reorientViewport(sf::Vector2f(y, x));
}

void InGameState::enterDungeon(bool moveToExit)
{
	if (moveToExit)
	{
		// Find the row and col of the dungeon entrance and teleport player there.
		for (unsigned int row = 0; row < maps[level]->tiles.size(); row++) 
			for (unsigned int col = 0; col < maps[level]->tiles.size(); col++) 
				if (static_cast<cotw::Tile*>(maps[level]->tiles[row][col])->name == "DungeonStairsUp")
				{
					teleportPlayer(row, col);
					//revealLos();
					cout << "Player was moved to row = " << row << " and col = " << col << endl;
					row = 30;
					col = 30;
				}
	}
}

void InGameState::reorientViewport(sf::Vector2f new_drawing_pos)
{
	cout << "Reorienting level = " << level << endl;

	for (unsigned int i = 0; i < 4; i++) 
		for (unsigned int row = 0; row < maps[i]->tiles.size(); row++) 
			for (unsigned int col = 0; col < maps[i]->tiles.size(); col++) 
				static_cast<cotw::Tile*>(maps[i]->tiles[row][col])->sprite.move(new_drawing_pos);
}

void InGameState::exitGame()
{
	window->close();
}

}
