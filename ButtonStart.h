#ifndef BUTTONSTART_H
#define BUTTONSTART_H

#include "Button.h"

namespace cotw {

class ButtonStart: public Button
{
	public:
		ButtonStart(sf::Texture&, std::string, sf::Vector2f, unsigned int, unsigned int);
		~ButtonStart();

		void update();
		void draw(sf::RenderTarget&, sf::RenderStates) const;
		void onClick(cotw::GameState*) override;
};

}

#endif
