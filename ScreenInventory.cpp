#include <iostream>
#include "ScreenInventory.h"

using namespace std;

namespace cotw {

ScreenInventory::ScreenInventory(sf::Texture& _texture, std::string _text, sf::Vector2f coords): visible(false)
{
	texture = _texture;
	//font.loadFromFile("DejaVuSans.ttf");
	sf::Vector2f text_coords = coords;
	text_coords.y += 8;
	text_coords.x += 10;
	name = "ScreenInventory";

	sprite.setTexture(texture);
	sprite.setPosition(coords); 
}

ScreenInventory::~ScreenInventory()
{

}

void ScreenInventory::update() 
{

}

void ScreenInventory::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{
	if (visible)
		render_target.draw(sprite, render_states);
}

void ScreenInventory::onClick(cotw::GameState* current_game_state)
{
	current_game_state->changeState(current_game_state->gameStateManager, cotw::State::MAINMENU);
}


}
