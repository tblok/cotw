#ifndef OVERWORLDMAP_H
#define OVERWORLDMAP_H

#include "Map.h"

namespace cotw {

class OverworldMap: public cotw::Map
{
	public:
		OverworldMap();
		~OverworldMap();

		void generate();
};

}

#endif
