#ifndef TEXTMANAGER_H
#define TEXTMANAGER_H

#include <SFML/Graphics.hpp>

namespace cotw {

class TextManager
{
	public:
		static Vector2f GetTextCenterPosition(float textPosition, float textSize, float textWidth) { 
			float horizontalMiddle = textPosition.x + (textSize.x / 2);
			float verticalPosition = textPosition.y + (textSize.y / 3);
			float horizontalPosition = horizontalMiddle - (textWidth / 2);

			sf::Vector2f centerPos;
			centerPos.y = verticalPosition;
			centerPos.x = horizontalPosition;

			return centerPos;
		};
};

}

#endif
