#include <iostream>
#include "OverworldRandomTileFactory.h"

using namespace std;

namespace cotw {

OverworldRandomTileFactory::OverworldRandomTileFactory() {}

OverworldRandomTileFactory::~OverworldRandomTileFactory() {}

std::vector<std::vector<cotw::Tile*>> OverworldRandomTileFactory::createTiles(const int row, const int col)
{
	std::string random_texture_name;
	std::string base_texture = "Grass1";
	sf::Texture base_texture_grass = textureManager.getTexture(base_texture, cotw::Atlas::game);
	bool blocking;
	bool tilesetCreationSuccess = false;
	std::vector<std::vector<cotw::Tile*>> tileset;
	
	do
	{
		random_texture_name = getRandomTileType();

		if (random_texture_name == "Log")
		{
			blocking = true;
			sf::IntRect tilesetCollisionData(col, row, 2, 1);

			if(tilesetCanBePlaced(tilesetCollisionData, row, col, 31, 31))
			{
				tilesetCollisionCollection.push_back(tilesetCollisionData);

				// @TODO: I feel uncomfortable about this code!
				std::string LogW;
				std::string LogE;
				const int color = cotw::getRandomIntegerBetween(0, 1);

				if (color == 1)
				{
					LogW = "LogW1";
					LogE = "LogE1";
				}
				else
				{
					LogW = "LogW2";
					LogE = "LogE2";
				}
				// end.

				tileset =
				{ 
					{
						new cotw::Tile
						(
							random_texture_name, 
						    textureManager.getMergedTexture(base_texture, LogW),
						    row * 32,
						    col * 32,
						    blocking
						),
						new cotw::Tile
						(
							random_texture_name,
							textureManager.getMergedTexture(base_texture, LogE),
							row * 32, 
							(col + 1) * 32, 
							blocking)
					}
				};

				return tileset;
			}
		}
		else if (random_texture_name == "TreeStumpLarge")
		{
			blocking = true;

			sf::IntRect tilesetCollisionData(col, row, 2, 2);
			if(tilesetCanBePlaced(tilesetCollisionData, row, col, 31, 31))
			{
				tilesetCollisionCollection.push_back(tilesetCollisionData);

				tileset =
				{ 
					{
						new cotw::Tile
						(
							random_texture_name,
							textureManager.getMergedTexture(base_texture, "TreeStumpLargeNw"),
							row * 32,
							col * 32,
							blocking
						), 
						new cotw::Tile
						(
							random_texture_name,
							textureManager.getMergedTexture(base_texture, "TreeStumpLargeNe"), 
							row * 32,
							((col + 1) * 32),
							blocking
						)
					},
					{
						new cotw::Tile
						(
							random_texture_name,
							textureManager.getMergedTexture(base_texture, "TreeStumpLargeSw"), 
							((row + 1) * 32),
							col * 32, 
							blocking
						),
						new cotw::Tile
						(
							random_texture_name,
							textureManager.getMergedTexture(base_texture, "TreeStumpLargeSe"),
							((row + 1) * 32),
							((col + 1) * 32), 
							blocking
						)
					}
				};

				return tileset;
			}
		}
		else if (random_texture_name == "MudPatch")
		{
			blocking = false;
			int max_height = 5;
			int max_width = 5;
			int min_height = 3;
			int min_width = 3;

			int rowsBeforeEndOfMap = 31 - row;
			int colsBeforeEndOfMap = 31 - col;

			if (rowsBeforeEndOfMap < max_height)
				max_height = rowsBeforeEndOfMap;
			if (colsBeforeEndOfMap < max_width)
				max_width = colsBeforeEndOfMap;

			bool validTileParameters = (max_height > min_height) && (min_height < max_height) && (max_width > min_width) && (min_width < max_width);
			if (validTileParameters)
			{
				unsigned int rand_height = cotw::getRandomIntegerBetween(min_width, max_width);
				unsigned int rand_width = cotw::getRandomIntegerBetween(min_width, max_width);

				sf::IntRect tilesetCollisionData(col, row, rand_width, rand_height);
				if(tilesetCanBePlaced(tilesetCollisionData, row, col, 31, 31))
				{
					tilesetCollisionCollection.push_back(tilesetCollisionData);

					for (unsigned int i = 0; i < rand_height; i++) 
						tileset.push_back(std::vector<cotw::Tile*>());

					for (unsigned int special_row = 0; special_row < rand_height; special_row++) 
						for (unsigned int special_col = 0; special_col < rand_width; special_col++) 
						{
							if ((special_row == 0) && (special_col == 0))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "MudEdgeNw"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if ((special_row == (rand_height - 1)) && (special_col == 0))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "MudEdgeSw"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if ((special_row == 0) && (special_col == (rand_width - 1)))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "MudEdgeNe"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if ((special_row == (rand_height - 1)) && (special_col == (rand_width - 1)))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "MudEdgeSe"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if ((special_row == 0) && (special_col > 0))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "MudEdgeN"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if ((special_row > 0) && (special_col == 0))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "MudEdgeW"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if (special_col == (rand_width - 1))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "MudEdgeE"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if (special_row == (rand_height - 1))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "MudEdgeS"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getTexture("Mud", cotw::Atlas::game), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
						}
							
					return tileset;
				}
			}
		}
		else if (random_texture_name == "Water2")
		{
			blocking = true;
			int max_height = 5;
			int max_width = 5;
			int min_height = 3;
			int min_width = 3;
			
			// So row is 29. Thats 31 - 29
			// go into if
			// max_height becomes 2

			int rowsBeforeEndOfMap = 31 - row;
			int colsBeforeEndOfMap = 31 - col;

			if (rowsBeforeEndOfMap < max_height)
				max_height = rowsBeforeEndOfMap;
			if (colsBeforeEndOfMap < max_width)
				max_width = colsBeforeEndOfMap;

			if ((max_height > min_height) && (min_height < max_height) && (max_width > min_width) && (min_width < max_width))
			{
				//bool validTileParameters;

				//do
				//{
				//	validTileParameters = (max_height > min_height) && (min_height < max_height) && (max_width > min_width) && (min_width < max_width);
				//}
				//while (validTileParameters);

				unsigned int rand_height = cotw::getRandomIntegerBetween(min_height, max_height);
				unsigned int rand_width = cotw::getRandomIntegerBetween(min_width, max_width);

				sf::IntRect tilesetCollisionData(col, row, rand_width, rand_height);
				if(tilesetCanBePlaced(tilesetCollisionData, row, col, 31, 31))
				{
					tilesetCollisionCollection.push_back(tilesetCollisionData);

					for (unsigned int i = 0; i < rand_height; i++) 
						tileset.push_back(std::vector<cotw::Tile*>());


					for (unsigned int special_row = 0; special_row < rand_height; special_row++) 
						for (unsigned int special_col = 0; special_col < rand_width; special_col++) 
						{
							if ((special_row == 0) && (special_col == 0))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "WaterEdgeNw"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if ((special_row == (rand_height - 1)) && (special_col == 0))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "WaterEdgeSw"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if ((special_row == 0) && (special_col == (rand_width - 1)))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "WaterEdgeNe"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if ((special_row == (rand_height - 1)) && (special_col == (rand_width - 1)))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "WaterEdgeSe"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if ((special_row == 0) && (special_col > 0))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "WaterEdgeN"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if ((special_row > 0) && (special_col == 0))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "WaterEdgeW"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if (special_col == (rand_width - 1))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "WaterEdgeE"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else if (special_row == (rand_height - 1))
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "WaterEdgeS"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
							else
								tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getTexture("Water2", cotw::Atlas::game), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
						}

					return tileset;
				}
			}

		}
		else if (random_texture_name == "GrassField")
		{
			blocking = false;
			int min_height = 1;
			int min_width = 1;
			int max_height = 5;
			int max_width = 5;

			if ((31 - row) < 5)
				max_height = 31 - row;
			if ((31 - col) < 5)
				max_width = 31 - col;

			unsigned int rand_height = cotw::getRandomIntegerBetween(min_height, max_height);
			unsigned int rand_width = cotw::getRandomIntegerBetween(min_width, max_width);

			sf::IntRect tilesetCollisionData(col, row, rand_width, rand_height);
			if(tilesetCanBePlaced(tilesetCollisionData, row, col, 31, 31))
			{
				tilesetCollisionCollection.push_back(tilesetCollisionData);
				int grass_clearing;

				for (unsigned int i = 0; i < rand_height; i++) 
					tileset.push_back(std::vector<cotw::Tile*>());

				for (unsigned int special_row = 0; special_row < rand_height; special_row++) 
					for (unsigned int special_col = 0; special_col < rand_width; special_col++) 
					{

						grass_clearing = cotw::getRandomIntegerBetween(0, 1);
						if (grass_clearing == 1)
						{
							tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, "GrassHigh"), ((row + special_row) * 32), ((col + special_col) * 32), blocking));
						}
						else
						{
							sf::Texture texture = base_texture_grass;
							randomlyFlipTextureH(texture);
							tileset.at(special_row).push_back(new cotw::Tile(random_texture_name, texture, ((row + special_row) * 32), ((col + special_col) * 32), blocking));
						}
					}

				return tileset;
			}
		}
		else if (random_texture_name == "TreeLarge")
		{
			blocking = true;
			sf::IntRect tilesetCollisionData(col, row, 2, 2);

			if(tilesetCanBePlaced(tilesetCollisionData, row, col, 31, 31))
			{
				tilesetCollisionCollection.push_back(tilesetCollisionData);
				const int different_color = cotw::getRandomIntegerBetween(0, 2);
				std::string TreeLargeNw = "TreeLargeNw";
				std::string TreeLargeNe = "TreeLargeNe";
				std::string TreeLargeSw = "TreeLargeSw";
				std::string TreeLargeSe = "TreeLargeSe";
				const std::string v2 = "V2";
				const std::string v3 = "V3";
				const std::string v4 = "V4";

				if (different_color == 0)
				{
					TreeLargeNw += v2;
					TreeLargeNe += v2;
					TreeLargeSw += v2;
					TreeLargeSe += v2;
				}
				else if (different_color == 1)
				{
					TreeLargeNw += v3;
					TreeLargeNe += v3;
					TreeLargeSw += v3;
					TreeLargeSe += v3;
				}
				else if (different_color == 2)
				{
					TreeLargeNw += v4;
					TreeLargeNe += v4;
					TreeLargeSw += v4;
					TreeLargeSe += v4;
				}

				tileset =
				{ 
					{
						new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, TreeLargeNw), row * 32, col * 32, blocking), 
						new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, TreeLargeNe), row * 32, ((col + 1) * 32), blocking)
					},
					{
						new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, TreeLargeSw), ((row + 1) * 32), col * 32, blocking),
						new cotw::Tile(random_texture_name, textureManager.getMergedTexture(base_texture, TreeLargeSe), ((row + 1) * 32), ((col + 1) * 32), blocking)
					}
				};

				return tileset;
			}
		}
		else if (random_texture_name == "BushLarge")
		{
			blocking = true;
			sf::IntRect tilesetCollisionData(col, row, 2, 1);
			if(tilesetCanBePlaced(tilesetCollisionData, row, col, 31, 31))
			{
				tilesetCollisionCollection.push_back(tilesetCollisionData);

				sf::Texture BushLargeW;
				sf::Texture BushLargeE;
				BushLargeW = textureManager.getMergedTexture(base_texture, "BushLargeW");
				BushLargeE = textureManager.getMergedTexture(base_texture, "BushLargeE");

				tileset =
				{ 
					{
						new cotw::Tile(random_texture_name, BushLargeW, row * 32, col * 32, blocking), 
						new cotw::Tile(random_texture_name, BushLargeE, row * 32, ((col + 1) * 32), blocking)
					}
				};

				return tileset;
			}
		}
		else 
		{
			sf::Texture texture;

			// Some tiles, like Grass1 can be flipped horizontally and vertically
			if (random_texture_name == "Grass1")
			{
				blocking = false;
				texture = textureManager.getTexture(base_texture, cotw::Atlas::game);
				randomlyFlipTexture(texture);
			}
			else
			{
				// Got to set the blocking somehow... must be refactored soon. Also this code must be in the else outside of this else because this else must trigger to return the tileset for a singular tile. If these ifs below were present in the big if statements above, this else would not trigger. The alternative would be letting the code reside beyond the if statements but that is hard to read.
				if (random_texture_name == "Poppy1")
					blocking = false;
				else if (random_texture_name == "Poppy2")
					blocking = false;
				else if (random_texture_name == "Fern1")
					blocking = false;
				else if (random_texture_name == "Fern2")
					blocking = false;
				else if (random_texture_name == "Fern3")
					blocking = false;
				else if (random_texture_name == "Fern4")
					blocking = false;
				else if (random_texture_name == "Mushrooms2")
					blocking = false;
				else if (random_texture_name == "Mushrooms3")
					blocking = false;
				else if (random_texture_name == "DEADPLANT1")
					blocking = false;
				else if (random_texture_name == "TreeStumpSmall")
					blocking = true;
				else if (random_texture_name == "RockSmall2")
					blocking = true;
				else if (random_texture_name == "RockMedium")
					blocking = true;
				else if (random_texture_name == "BushSmall1")
					blocking = true;

				texture = textureManager.getMergedTexture(base_texture, random_texture_name);
				randomlyFlipTextureH(texture);
			}

			// Wrap the singular texture in a vector to comply with method signature...
			tileset =
			{ 
				{
					new cotw::Tile(random_texture_name, texture, row * 32, col * 32, blocking)
				}
			};

			return tileset;
		}
	}
	while (tilesetCreationSuccess == false);

	if (tileset.size() == 0)
	{
		cout << "#### WARNING!!! Returning from createTiles() factory method but tiles is empty!" << endl;
	}

	return tileset;
}

}
