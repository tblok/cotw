#ifndef EXPERIMENTALBUTTON_H
#define EXPERIMENTALBUTTON_H

#include "IUpdateable.h"
#include "IClickable.h"
#include "Entity.h"
#include <SFML/Graphics.hpp>
#include "GameState.h"
#include "InGameState.h"

namespace cotw {

class ExperimentalButton : public Entity, public IUpdateable, public IClickable
{
	public:
		ExperimentalButton();
		ExperimentalButton(sf::Texture&, std::string, sf::Vector2f, sf::Vector2u);
		~ExperimentalButton();

		sf::Text text;
		sf::Font font;

		void onClick(cotw::GameState*);
		void update();
};

}

#endif
