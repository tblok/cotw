#ifndef DUNGEONRANDOMTILEFACTORY_H
#define DUNGEONRANDOMTILEFACTORY_H

#include "Tile.h"
#include "TileFactory.h"

namespace cotw {

class DungeonRandomTileFactory: public cotw::TileFactory
{
	public:
		DungeonRandomTileFactory();
		~DungeonRandomTileFactory();

		std::vector<std::vector<cotw::Tile*>> createTiles(const int, const int);
};

}

#endif
