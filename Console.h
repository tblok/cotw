#ifndef CONSOLE_H
#define CONSOLE_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Drawable.h"
#include "GameState.h"
#include "IClickable.h"

namespace cotw {

class Console: public cotw::Drawable, public cotw::IClickable
{
	public:
		Console(sf::Texture&, std::string, sf::Vector2f/*, unsigned int, unsigned int*/);
		~Console();

		//sf::Rect hitbox;
		std::array<sf::Text, 6> texts;
		sf::Font font;
		std::vector<std::string> messages;

		void update();
		void log(std::string, sf::Color);
		void draw(sf::RenderTarget&, sf::RenderStates) const;
		void onClick(cotw::GameState*);
};

}

#endif
