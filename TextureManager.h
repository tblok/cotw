#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <unordered_map>
// Jsoncpp
#include <fstream>
#include <jsoncpp/json/json.h>
#include "JsonReader.h"

namespace cotw {

enum class Atlas { game, ui };

class TextureManager
{
	public:
		TextureManager();
		~TextureManager();

		sf::Texture& getTexture(std::string, cotw::Atlas);
		sf::Texture& getMergedTexture(std::string, std::string);
		sf::Texture mergeTexture(sf::Texture, sf::Texture);
	private:
		std::unordered_map<std::string, sf::Texture> textures;
		void loadAllTextures();
};

}

#endif
