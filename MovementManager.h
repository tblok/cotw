#ifndef MOVEMENTMANAGER_H
#define MOVEMENTMANAGER_H

#include <SFML/Graphics.hpp>


namespace cotw {

class MovementManager
{
	public:
		bool validMove(sf::Vector2<unsigned int>);
		void teleportPlayer(int, int);
		MovementManager();
		~MovementManager();
};

}

#endif
