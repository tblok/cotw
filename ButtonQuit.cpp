#include <iostream>
#include "ButtonQuit.h"

using namespace std;

namespace cotw {

ButtonQuit::ButtonQuit(sf::Texture& _texture, std::string _text, sf::Vector2f coords, unsigned int _width, unsigned int _height) 
{
	texture = _texture;
	name = "Quit";

	font.loadFromFile("res/8bit.ttf");
	text.setFont(font);
	text.setCharacterSize(30);
	text.setStyle(sf::Text::Bold);
	text.setFillColor(sf::Color::White);
	text.setString(_text);

	sprite.setTexture(texture);
	sprite.setPosition(coords); 
	center_text();
}

ButtonQuit::~ButtonQuit(){}

void ButtonQuit::update()
{

}

void ButtonQuit::draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const
{
	render_target.draw(sprite, render_states);
	render_target.draw(text, render_states);
}

void ButtonQuit::onClick(cotw::GameState* current_game_state)
{
	current_game_state->exitGame();
}

}
