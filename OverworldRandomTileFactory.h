#ifndef OVERWORLDRANDOMTILEFACTORY_H
#define OVERWORLDRANDOMTILEFACTORY_H

#include "Tile.h"
#include "TileFactory.h"

namespace cotw {

class OverworldRandomTileFactory: public cotw::TileFactory
{
	public:
		OverworldRandomTileFactory();
		~OverworldRandomTileFactory();

		std::vector<std::vector<cotw::Tile*>> createTiles(const int, const int);

};

}

#endif
