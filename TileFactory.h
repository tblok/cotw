#ifndef TILEFACTORY_H
#define TILEFACTORY_H

#include "Tile.h" 
#include <iostream> 
#include <string>
#include "TextureManager.h"
#include "Drawable.h"

namespace cotw {

int getRandomIntegerBetween(int, int);

class TileFactory
{
	public:
		virtual std::vector<std::vector<cotw::Tile*>> createTiles(const int, const int) = 0;

	protected:
		std::vector<sf::IntRect> tilesetCollisionCollection;
		cotw::TextureManager textureManager;

		void randomlyFlipTexture(sf::Texture& texture)
		{
			randomlyFlipTextureH(texture);
			randomlyFlipTextureV(texture);
		}

		void randomlyFlipTextureH(sf::Texture& texture)
		{
			const int flip_h = cotw::getRandomIntegerBetween(0, 1);
			if (flip_h == 1)
			{
				sf::Image image = texture.copyToImage();
				image.flipHorizontally();
				if (!texture.loadFromImage(image))
					std::cout << "texture was not found!" << std::endl; 
			}
		}

		void randomlyFlipTextureV(sf::Texture& texture)
		{
			const int flip_v = cotw::getRandomIntegerBetween(0, 1);
			if (flip_v == 1)
			{
				sf::Image image = texture.copyToImage();
				image.flipVertically();
				if (!texture.loadFromImage(image))
					std::cout << "texture was not found!" << std::endl; 
			}
		}


		bool tilesetCanBePlaced(const sf::IntRect tileset, const unsigned int row, const unsigned int col, const unsigned int map_height, const unsigned int map_width) const
		{
			// Code for checking if tileset extends beyond map
			if (tilesetFitsInMap(tileset, row, col, map_width, map_height) == false)
				return false;

			bool intersects = false;

			for (unsigned int i = 0; i < tilesetCollisionCollection.size(); i++) 
			{
				if (tileset.intersects(tilesetCollisionCollection.at(i)))
				{
					intersects = true;
					break;
				}
			}

			return intersects == false;
		}

		/**
		 * In a for loop two variables are kept. The first is 0 or the previous value of the tileSetSpawnRates, the second is current value of the tileSetSpawnrates. 
		 * The idea is that if the random float becomes 7.4 then the algorithm knows 7.4 is between the 0-10.0 range of the TreeLarge key range, thus TreeLarge is selected. 
		 * Similarly, the very next range in sequence will be 10.0-10.2, which means that if the random float happens to be between 10.0 and 10.2, Log will be chosen. 
		 * Therefore TreeLarge has a 10% spawn chance and Log has a 0.2% spawn chance.
		 */
		std::string getRandomTileType() const
		{
			std::vector<std::pair<std::string, float>> spawnrates = 
			{
				{"TreeLarge", 2.0},
				{"Log", 0.2},
				{"MudPatch", 0.2},
				{"GrassField", 2.0},
				{"Water2", 1.0},
				{"BushSmall1", 2.0},
				{"BushSmall2", 1.0},
				{"BushSmall3", 1.0},
				{"BushLarge", 0.5},
				{"TreeStumpSmall", 1.0},
				{"RockMedium", 1.0},
				{"RockSmall1", 1.5},
				{"Mushrooms2", 0.75},
				{"Mushrooms3", 0.75},
				{"Poppy2", 2.0},
				{"Fern2", 2.0},
				{"Fern3", 2.0},
				{"Fern4", 2.0},
			};

			float total = 0;
			for (auto it : spawnrates) 
				total += it.second;

			float remaining_percentage = 100.0 - total;
			spawnrates.push_back(std::pair<std::string, float>("Grass1", remaining_percentage));

			const float rand_spawn_chance = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 100));

			float curr = 0.0;
			float total_curr = 0.0;
			float spawncount_prev = 0.0;

			bool first_loop = true;
			std::string tile_type;

			for (auto it : spawnrates) 
			{
				// spawncount_prev has to always remain one value before the curr, that's why I use the "curr" here of the previous loop.
				if (!first_loop)
					spawncount_prev += curr;

				curr = it.second;
				total_curr += it.second;
				if ((rand_spawn_chance >= spawncount_prev) && (rand_spawn_chance <= total_curr))
				{
					tile_type = it.first;
					return tile_type;
				}

				if (first_loop)
					first_loop = false;
			}

			return "Grass1";
		}

	private:
		bool tilesetFitsInMap(const sf::IntRect tileset, const unsigned int row, const unsigned int col, const unsigned int map_width, const unsigned int map_height) const
		{
			if ((col + tileset.width > map_width) || (row + tileset.height > map_height))
				return false;
			else
				return true;
		}
		
};

}

#endif
