#include <iostream>
#include "OverworldMap.h"

using namespace std;

namespace cotw {

OverworldMap::OverworldMap() 
{
	tileFactory = new OverworldRandomTileFactory();
	tiles = { };
	srand(time(0));
}

OverworldMap::~OverworldMap()
{
	for (unsigned int y = 0; y < tiles.size(); y++) 
		for (unsigned int x = 0; x < tiles.size(); x++) 
			delete tiles[y][x];
}

void OverworldMap::generate() 
{
	for (unsigned int row = 0; row < 31; row++) 
	{
		for (unsigned int col = 0; col < 31; col++) 
		{
			if (tiles[row][col] == NULL)
			{
				std::vector<std::vector<cotw::Tile*>> randomTileset = tileFactory->createTiles(row, col);
				placeTiles(randomTileset, row, col);
			}
		}
	}

	createUniqueTile("DungeonEntrance", "Grass1");
}

}
