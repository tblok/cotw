#include <iostream>
#include "DungeonMap.h"

using namespace std;

namespace cotw {

DungeonMap::DungeonMap() 
{
	tileFactory = new DungeonRandomTileFactory();
	tiles = { };
	srand(time(0));
}

DungeonMap::~DungeonMap()
{
	for (unsigned int y = 0; y < tiles.size(); y++) 
		for (unsigned int x = 0; x < tiles.size(); x++) 
			delete tiles[y][x];
}

void DungeonMap::generate() 
{
	for (unsigned int row = 0; row < tiles.size(); row++) 
		for (unsigned int col = 0; col < tiles.size(); col++) 
			tiles[row][col] = new Tile("RockWall", textureManager.getTexture("RockWall", cotw::Atlas::game), row * 32, col * 32, true);
	createRooms();

	for (unsigned int i = 0; i < rooms.size(); i++) 
		createTunnel(rooms[i]);

	//for (unsigned int row = 0; row < tiles.size(); row++) 
	//	for (unsigned int col = 0; col < tiles.size(); col++) 
	//		static_cast<cotw::Tile*>(tiles[row][col])->conceal();

	////if (level != 3) disabled for now
	// Kind of bad... second param tile must exist or endless loop
	createUniqueTile("DungeonStairsDown", "RockGround");
	createUniqueTile("DungeonStairsUp", "RockGround");
}

void DungeonMap::digTunnels() 
{
	// probably gotta be 31 both
	int row = getRandomIntegerBetween(0, 30);
	int col = getRandomIntegerBetween(0, 30);
	row = 15;
	col = 15;

	//cout <<  row << " and col: " << col << endl;

	std::string tile_type = "RockGround";
	static_cast<cotw::Tile*>(tiles[row][col])->sprite.setTexture(textureManager.getTexture(tile_type, cotw::Atlas::game));
	static_cast<cotw::Tile*>(tiles[row][col])->name = tile_type;
	static_cast<cotw::Tile*>(tiles[row][col])->blocking = 0;

	int direction;
	bool firstTime = true;

	for (int i = 0; i < 10; i++) 
	{
		if (firstTime)
		{
			direction = getRandomIntegerBetween(0, 7);
			direction = 6;
			firstTime = false;
		}
		else 
		{
			int rand;
			do  
			{
				rand = getRandomIntegerBetween(0, 7);
			} 
			while (rand == direction);

			direction = rand;
		}

		switch (direction) 
		{
			case 0:
			{
				cout << "0" << endl;
			}
			break;
			case 1: // north
			{
				// Digger stops at 1 tile before the outer edge.
				while ((unsigned int)(row - 1) > 0)
				{
					row--;
					static_cast<cotw::Tile*>(tiles[row][col])->sprite.setTexture(textureManager.getTexture("RockGround", cotw::Atlas::game));
					static_cast<cotw::Tile*>(tiles[row][col])->blocking = 0;
				}
			}
			break;
			case 2:
			{
				cout << "2" << endl;
			}
			break;
			case 3: // left
			{
				cout << "3" << endl;
			}
			break;
			case 4: // right
			{
				cout << "4" << endl;
			}
			break;
			case 5:
			{
				cout << "5" << endl;
			}
			break;
			case 6: // south
			{

				// Digger stops at 1 tile before the outer edge.
				while ((unsigned int)(row + 1) < (unsigned int)(tiles.size() - 1))
				{
					row++;
					static_cast<cotw::Tile*>(tiles[row][col])->sprite.setTexture(textureManager.getTexture("RockGround", cotw::Atlas::game));
					static_cast<cotw::Tile*>(tiles[row][col])->blocking = 0;
				}
				cout << "6" << endl;
			}
			break;
			case 7:
			{
				cout << "7" << endl;
			}
			break;

			default:
				cout << "default" << endl;
			break;

		}

		if (!firstTime)
			firstTime = false;
	}


	//for (int col = start_col; col < end_col + 1; col++) 
	//{
	//	static_cast<cotw::Tile*>(tiles[row][col])->sprite.setTexture(textureManager.getTexture("RockGround", cotw::Atlas::game));
	//	static_cast<cotw::Tile*>(tiles[row][col])->blocking = 0;
	//}
}

void DungeonMap::createRooms() 
{
	int min_room_width = 2;
	int min_room_height = 2;
	int max_room_width = 5;
	int max_room_height = 5;

	bool does_not_intersect;
	for (unsigned int i = 0; i < rooms.size(); i++) 
	{
		int rand_row;
		int rand_column;
		int rand_height;
		int rand_width; 

		do
		{
			does_not_intersect = true;
			rand_row = getRandomIntegerBetween(1, 30);
			rand_column = getRandomIntegerBetween(1, 30);
			rand_height = getRandomIntegerBetween(min_room_height, max_room_height + 1);
			rand_width = getRandomIntegerBetween(min_room_width, max_room_width + 1);

			//rand_row = (rand() % 30) + 1;
			//rand_column = (rand() % 30) + 1;
			//rand_height = rand() % (max_room_height - min_room_height + 1) + min_room_height;
			//rand_width = rand() % (max_room_width - min_room_width + 1) + min_room_width;
			sf::IntRect room(rand_column, rand_row, rand_width, rand_height);

			for (unsigned int h = 0; h < rooms.size(); h++)  
			{
				if (room.intersects(rooms[h]))
					does_not_intersect = false;
				if ( ((room.left + room.width) > 30) || ((room.top + room.height) > 30) )
					does_not_intersect = false;
			}
		}
		while (!does_not_intersect);
		
		//rand_width = 5; 
		//rand_height = 5;

		// Manually create the first 5 rooms
		//if (i == 0)
		//{
		//	rand_row = 1;
		//	rand_column = 1;
		//}
		//else if (i == 1)
		//{
		//	rand_row = 1;
		//	rand_column = 10;
		//}
		//else if (i == 2)
		//{
		//	rand_row = 10;
		//	rand_column = 10;
		//}
		//else if (i == 3)
		//{
		//	rand_row = 10;
		//	rand_column = 1;
		//}
		//else if (i == 4)
		//{
		//	rand_row = 10;
		//	rand_column = 20;
		//}

		rooms[i] = sf::IntRect(rand_column, rand_row, rand_width, rand_height);
		createRoom(rooms[i], i);
	}
}

void DungeonMap::createRoom(sf::IntRect room, int index) 
{
	for (int iy = 0; iy < room.width; iy++)
	{
		for (int ix = 0; ix < room.height; ix++)
		{
			if (((unsigned int)(room.left + iy) < tiles.size()) && ((unsigned int)(room.top + ix) < tiles.size()))
			{
				std::string tile_type = "RockGround";
				static_cast<cotw::Tile*>(tiles[room.top + ix][room.left + iy])->originalTexture = textureManager.getTexture(tile_type, cotw::Atlas::game);
				static_cast<cotw::Tile*>(tiles[room.top + ix][room.left + iy])->sprite.setTexture(textureManager.getTexture(tile_type, cotw::Atlas::game));
				static_cast<cotw::Tile*>(tiles[room.top + ix][room.left + iy])->name = tile_type;
				static_cast<cotw::Tile*>(tiles[room.top + ix][room.left + iy])->blocking = 0;
			}

		}
	}
}

const std::vector<sf::IntRect> DungeonMap::getUnconnectedRooms() 
{
	std::vector<int> connected_rooms;

	for (unsigned int i = 0; i < rooms.size(); i++) 
	{
		if ((i + 1) < rooms.size())
		{
			sf::IntRect room = rooms[i];
			room.left -= 1;
			room.top -= 1;
			room.width += 2;
			room.height += 2;

			for (unsigned int h = 0; h < rooms.size(); h++) 
				if (h != i)
					if (room.intersects(rooms[h]))
					{
						connected_rooms.push_back(i);
						
						if (h == (rooms.size() - 1))
						{
							connected_rooms.push_back(h);
						}

						++h;
					}

		}
	}

	std::vector<sf::IntRect> unconnected_rooms;

	for (unsigned int i = 0; i < rooms.size(); i++) 
	{
		if (!(std::find(connected_rooms.begin(), connected_rooms.end(), i) != connected_rooms.end()))
		{
			unconnected_rooms.push_back(rooms[i]);
		}
	}


	return unconnected_rooms;

}

void DungeonMap::createHTunnel(int start_col, int end_col, int row) 
{
	for (int col = start_col; col < end_col + 1; col++) 
	{
		std::string tile_type = "RockGround";
		static_cast<cotw::Tile*>(tiles[row][col])->originalTexture = textureManager.getTexture(tile_type, cotw::Atlas::game);
		static_cast<cotw::Tile*>(tiles[row][col])->sprite.setTexture(textureManager.getTexture(tile_type, cotw::Atlas::game));
		static_cast<cotw::Tile*>(tiles[row][col])->blocking = 0;
	}
}

void DungeonMap::createVTunnel(int start_row, int end_row, int col) 
{
	for (int row = start_row; row < end_row; row++) 
	{
		std::string tile_type = "RockGround";
		static_cast<cotw::Tile*>(tiles[row][col])->originalTexture = textureManager.getTexture(tile_type, cotw::Atlas::game);
		static_cast<cotw::Tile*>(tiles[row][col])->sprite.setTexture(textureManager.getTexture(tile_type, cotw::Atlas::game));
		static_cast<cotw::Tile*>(tiles[row][col])->blocking = 0;
	}
}

// Sole responsibility of this method is to connect this room to ANY other room
void DungeonMap::createTunnel(sf::IntRect room) 
{
	// Scanning algorithm NORTH
	// --------------------------------------------------------------
	room.top -= 1;
	for (int i = room.left; i < (room.left + room.width); i++) 
	{
		for (int h = room.top; h > 0; h--) 
		{
			for (unsigned int g = 0; g < tiles.size(); g++) 
			{
				if (static_cast<cotw::Tile*>(tiles[h][g])->blocking == 0)
				{
					//cout << "At tiles[" << h << "][" << g << "] there is a open room." << endl;
				}
			}

			if (static_cast<cotw::Tile*>(tiles[h - 1][i])->blocking == 0)
			{
				//cout << "Making tunnel NORTH" << endl;
				createVTunnel(h, room.top + 1, i);
				return;
			}
		}
	}
	room.top += 1;

	// Scanning algorithm EAST
	// --------------------------------------------------------------
	for (int row = room.top; row < (room.top + room.height); row++) 
	{
		for (unsigned int col = (room.left + room.width); col < tiles.size(); col++) 
		{
			//cout << "Checking tiles[" << row << "][" << col << "]." << endl;
			if ((col + 1) > tiles.size() - 1)
			{
				row = room.top + room.height;
				col = tiles.size();
			}
			else if (static_cast<cotw::Tile*>(tiles[row][col + 1])->blocking == 0)
			{
				//cout << "Making tunnel EAST" << endl;
				createHTunnel(room.left + room.width, col, row);
				return;
			}

		}
	}
	// --------------------------------------------------------------

	// Scanning algorithm WEST
	// --------------------------------------------------------------
	room.left -= 1;
	for (int row = room.top; row < (room.left + room.height); row++) 
	{
		for (int col = room.left; col > 0; col--) 
		{
			if (static_cast<cotw::Tile*>(tiles[row][col - 1])->blocking == 0)
			{
				//cout << "Making tunnel WEST" << endl;
				createHTunnel(col, room.left, row);
				return;
			}

		}
	}
	// --------------------------------------------------------------
	// Scanning algorithm SOUTH
	// --------------------------------------------------------------
	//cout << room.left << " to " << room.left + room.width << endl;
	for (int i = room.left; i < room.left + room.width; i++) 
	{
		for (unsigned int h = room.top + room.height; h < tiles.size(); h++) 
		{
			//cout << "Checking tiles[" << h << "][" << i << "]." << endl;
			if ((h + 1) == tiles.size())
			{
				i = room.left + room.width;
				h = tiles.size();
			}
			else if (static_cast<cotw::Tile*>(tiles[h + 1][i])->blocking == 0)
			{
				//cout << "Making tunnel SOUTH" << endl;
				createVTunnel(room.top + room.height, h + 1, i);
				return;
			}
		}
	}
}	

}
