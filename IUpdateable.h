#ifndef IUPDATEABLE_H
#define IUPDATEABLE_H

namespace cotw {

class IUpdateable
{
	public:
		virtual ~IUpdateable() {};
		virtual void update() = 0;
};

}

#endif
