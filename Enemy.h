#ifndef ENEMY_H
#define ENEMY_H

#include "Drawable.h"
#include "Item.h"
#include <unordered_map>
#include "Pos.h"

namespace cotw {

class Enemy: public cotw::Drawable
{
	public:
		Enemy();
		Enemy(sf::Texture&, cotw::Pos);
		~Enemy();

		std::vector<cotw::Item*> inventory;
		int speed = 32;
		int row;
		int col;

		void update();
		void setDirection(cotw::Direction);
		void move(sf::Vector2i);
};

}

#endif
