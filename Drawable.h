#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <SFML/Graphics.hpp>

namespace cotw {

enum class Direction { NORTH, EAST, SOUTH, WEST };

class Drawable: public sf::Drawable
{
	public:
		Drawable() {};
		virtual ~Drawable() {}; // Good if you want to delete pointers to this abstract class.

		std::string name; // Only for debug

		virtual void draw(sf::RenderTarget& render_target, sf::RenderStates render_states) const { render_target.draw(sprite, render_states); };
		virtual void update() = 0;
		//virtual void interact();
		sf::Sprite sprite;

	protected:
		sf::Texture texture;
};

}

#endif
